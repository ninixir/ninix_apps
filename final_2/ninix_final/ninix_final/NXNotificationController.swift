//
//  NXNotificationController.swift
//  ninix_final
//
//  Created by Ali on 1/31/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXNotificationController: NXModelController<NXNotificationModel>, SyncAbleModel {
    
    typealias Model = NXNotificationModel
    
    func readAllNotifications() {
        do {
            try realm.write {
                for notification in all() {
                    notification.seen = true
                }
            }
        }
        catch let e as NSError {
            print("error occured to init realm ", e, " <NotificationHnadler:readAllNotifications>")
        }
        
    }
    
    func unreadNotificationCount() -> Int {
        return all().filter("seen = false").count
    }
    
    func save(object: NXNotificationModel) {
        print("save notification start")
        
        if let lastEntryDate = last?.time as? Date {
            print(" find last notification")
            if lastEntryDate > Date(timeIntervalSinceNow: -30) {
                return
            }
        }
 
        super.save(object: object)
        let _ = NXNotificationEvent(notificationModel: object)
        
        
        
    }
    

}
