//
//  NXPeripheral.swift
//  ninix_final
//
//  Created by Ali on 1/21/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth
import ReactiveKit
import Bond

class NXPeripheral: NSObject, CBPeripheralDelegate {
    
    static let instance: NXPeripheral = NXPeripheral()
    
    var peripheral: CBPeripheral?
    var delegate: NXPeripheralDelegate?
    var _UUID: UUID?
    var timer: Timer!
    
    public func peripheralConnect(_ peripheral: CBPeripheral) {
        self.timer = Timer.scheduledTimer(timeInterval: NXHealthStandard.thresholdTimeToSave, target: NXDataController.self, selector: #selector(NXDataController.handleLastData(_:)), userInfo: nil, repeats: true)
        let timerQueue = DispatchQueue(label: "com.ninixco.timer", attributes: .concurrent)
        timerQueue.async {
            self.timer.fire()
        }
        
        peripheral.delegate = self
        peripheral.discoverServices(NXUUID.services)
        self.peripheral = peripheral
        self._UUID = peripheral.identifier
        
    }
    
    public func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        
    }
    
    public func peripheralDidUpdateRSSI(_ peripheral: CBPeripheral, error: Error?) {
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        print("discovered new service <NXPeripheral:peripheral>")
        
        if let services = peripheral.services {
            for service in services {
                if NXUUID.services.contains(service.uuid) {
                    print("service container true <NXPeripheral:peripheral>")
                    peripheral.discoverCharacteristics(NXUUID.characteristics, for: service)
                }
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        print("discovered characteristic <NXPeripheral:peripheral>")
        
        if let characteristics = service.characteristics {
            for characteristic in characteristics {
                if NXUUID.characteristics.contains(characteristic.uuid) {
                    
                    print("characteristic container <NXPeripheral:peripheral>")
                    
                    peripheral.readValue(for: characteristic)
                    peripheral.setNotifyValue(true, for: characteristic)
                }
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print(NXLastReceivedData.time ?? "not assign yet", "last received time")
        if let time = NXLastReceivedData.time {
            print(-time.timeIntervalSinceNow, "time interval")
        }
        let bluetoothQueue = DispatchQueue(label: "com.ninixco.bluetooth", attributes: .concurrent)
        bluetoothQueue.async {
            guard let value = characteristic.value else {
                return
            }
            
            let parseQueue = DispatchQueue(label: "com.ninixco.parse", attributes: .concurrent)
            parseQueue.async {
                let _ = NXDataController(packet: value, delegate: self.delegate)
            }
 
        }
        
        
        
        
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        
    }
    
    
    
    public static func getInstance() -> NXPeripheral {
        return self.instance
    }
    
}
