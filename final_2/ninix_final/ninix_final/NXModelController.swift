//
//  NXModelController.swift
//  ninix_final
//
//  Created by Ali on 1/31/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXModelController<Model: Object> {
    
    var realm: Realm! 
    var last: Model? {
        get {
            return all().sorted(byKeyPath: "time", ascending: true).last
        }
    }
    
    
    init() {
        realm = try! Realm()
    }
    
    func save(object: Object) {
        
        do {
            try self.realm.write() {
                self.realm.add(object)
            }
        }
        catch let e as NSError {
            print(e, "error occured when saving data")
        }
    }
    
    func all() -> Results<Model> {
        return realm.objects(Model.self)
    }
    
    
}

class NXAbnormalController<Model: NXAbNormalModel>: NXModelController<Model> {
    
    var time: TimeInterval!
    
    init(time: TimeInterval) {
        super.init()
        self.time = time
    }
    
    func abnormals(ascending: Bool) -> Results<Model> {
        let abnormals = realm.objects(Model.self).filter("invalid = false AND didNotify = false AND valid = false").sorted(byKeyPath: "time", ascending: ascending)
        return abnormals
    }
    
    func invalidateRecord(_ object: Model) {
        do {
            try realm.write {
                object.invalid = true
            }
        }
        catch let e as NSError {
            print(e, "error invalidating object <NXAbnormalController:invalidateRecords>")
        }
    }
    
    func validateRecord(date: NSDate) {
        do {
            let objects = realm.objects(Model.self).filter("time >= %@", date)
            try realm.write {
                for object in objects {
                    object.valid = true
                }
                
            }
        }
        catch let e as NSError {
            print(e, "error invalidating object <NXAbnormalController:invalidateRecords>")
        }
    }
    
    // TODO : remove all invalid data
    
    
    func getAnalyzedValue(object: Model) -> Double? {
        
        save(object: object)
        
        let abnormals = self.abnormals(ascending: true)
        
        for abnormal in abnormals {
            
            let timeDifference = object.time.timeIntervalSince1970 - abnormal.time.timeIntervalSince1970
            
            if timeDifference < time {
                break;
            }
            
            if timeDifference > (time + NXHealthStandard.thresholdTimeToSave) {
                self.invalidateRecord(abnormal)
                continue
            }
            let average = NXVitalSignsController().average(date: abnormal.time, property: "temperature")
            self.validateRecord(date: abnormal.time)
            
            return average
        }
        
        return nil
    }
    
    
}


protocol SyncAbleModel {
    
    associatedtype Model: NXShouldSyncModel
    
    static var lastSyncedTime: Date? {get}
    
    static func didNotSync() -> Results<Model>
    static func didNotSyncNumber() -> Int
    static func sync(time: Date) -> Bool
}

extension SyncAbleModel {
    
    
    static var lastSyncedTime: Date? {
        get {
            return didNotSync().sorted(byKeyPath: "time", ascending: false).last?.time as Date?
        }
    }
    
    static func didNotSync() -> Results<Model> {
        let realm: Realm = try! Realm()
        return realm.objects(Model.self).filter("didSync = false")
        
    }
    
    
    static func didNotSyncNumber() -> Int {
        return didNotSync().count
    }
    
    static func sync(time: Date) -> Bool {
        
        print("start sync")
        let realm: Realm = try! Realm()
        do {
            print("realm initilzed")
            let objects = realm.objects(Model.self).filter("time <= %@", time)
            
            try realm.write {
                for object in objects {
                    object.didSync = true
                }
            }
            return true
        }
        catch let e as NSError {
            print(e, "error <NXModelController:sync>")
            return false
        }
    }
    
    
}





