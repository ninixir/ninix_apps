//
//  NXLoginHandler.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import Alamofire
import KeychainSwift

class NXLoginHandler: InternetService {
    
    let address: String = NXWebServiceAddress.login
    var username: String
    var password: String
    var delegate: NXLoginHandlerDelegate?
    
    init(username: String, password: String, delegate: NXLoginHandlerDelegate? = nil) {
        
        self.username = username
        self.password = password
        self.delegate = delegate
       
    }
    
    func request() {
        sessionManager.request(self.address, method: .post, parameters: self.parameters(), encoding: JSONEncoding.default).validate().responseObject(queue: self.queue) { (response: DataResponse<NXLoginJson>) in
            
            switch response.result {
                
            case .success:
                print(response.result.value ?? "nil", "response result value <NXSignupHandler:request>")
                self.validateResponse(response: response.result.value)
            case .failure(let error) :
                print(error, " error <NXSignupHandler:request>")
                self.delegate?.didReceiveError(error: error.localizedDescription)

            }
            
        }
    }
    
    
    
    func parameters() -> Parameters {
        let parameters: Parameters = [
            "username": self.username,
            "password": self.password
        ]
        
        return parameters
    }
    
    func validateResponse(response: NXLoginJson?) {
        guard let response = response, let code = response.code else {
            delegate?.didReceiveServerError()
            return
        }
        
        if code == 0, let jsonToken = response.result, let nxTokenHandler = NXTokenHandler(jsonToken: jsonToken) {
            nxTokenHandler.save()
            self.save()
            AppStateService.userLoggedIn()
            delegate?.didLogin()
            return
        }
        else {
            if let message = response.message_en {
                delegate?.didReceiveError(error: message)
                return
            }
        }
        delegate?.didReceiveServerError()
    }
    
    func save() {
        UserCredential.save(username: self.username, password: self.password)
    }
    
}

protocol NXLoginHandlerDelegate: InternetServiceDelegate {
    func didLogin()
}
