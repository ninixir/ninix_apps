//
//  TestInternetService.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import Alamofire

class InternetService {
    
    let sessionManager: SessionManager!
    let queue: DispatchQueue!
    
    init() {
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = NXWebServiceSettings.timeout
        sessionManager = Alamofire.SessionManager(configuration: configuration)
        queue = DispatchQueue.global(qos: .utility)
        
    }
}

protocol InternetServiceDelegate {
    
    func didReceiveServerError()
    func didReceiveError(error: String)
    
}

protocol AuthenticationHeader {
    var header: HTTPHeaders? { get }
}

extension AuthenticationHeader {
    var header: HTTPHeaders? {
        get {
            guard let token = NXTokenHandler.get() else {
                Router.routeToLogin()
                return nil
            }
            return [
                "Accept": "application/json",
                "Authorization": token
            ]
        }
    }
}


