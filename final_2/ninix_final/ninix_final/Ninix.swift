//
//  Ninix.swift
//  ninix_final
//
//  Created by Ali on 12/31/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth

class Ninix: CBPeripheralDelegate, CBCenteralDelegate {
    static let instance : Ninix = Ninix()
    
    private init() {
        
    }
    
    public static func instance() -> Ninix {
        return self.instance
    }
    
    
}
