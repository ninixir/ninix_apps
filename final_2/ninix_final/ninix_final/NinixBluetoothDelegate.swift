//
//  NinixBluetoothDelegate.swift
//  ninix_final
//
//  Created by Ali on 12/31/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import Foundation

struct BluetoothConnectionInstance {
    static var bluetoothConnection: DeviceViewController?
}

protocol NinixBluetoothDelegate {
    func didRecieveData(data: String)
}
