//
//  FancyTextField.swift
//  ninix_final
//
//  Created by Ali on 1/11/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class FancyTextField: UITextField {
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let width = CGFloat(0.5)
        let border = CALayer()
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderColor = UIColor.white.cgColor
        border.borderWidth = width
        
        self.font = UIFont(name: "Arima Madurai", size: 17.0)
        self.textColor = UIColor.white
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: UIColor.white])
        
    }
    
    
}
