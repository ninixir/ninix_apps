//
//  GradientView.swift
//  ninix_final
//
//  Created by Ali on 1/11/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
    var gradientColor: CAGradientLayer!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.gradientColor = ColorPallete.primaryGradient
        self.gradientColor.frame = self.bounds
        self.layer.insertSublayer(gradientColor, at: 0)
        
        self.backgroundColor = UIColor.clear
    }
    
    
}
