//
//  RegexMatch.swift
//  ninix_final
//
//  Created by Ali on 2/6/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class RegexMatch: ValidationRule {
    var error: String? = nil
    var pattern: String!
    var patternDescription: String!
    
    init(pattern: String, patternDescription: String) {
        self.pattern = pattern
        self.patternDescription = patternDescription
    }
    
    func validate(value: String) -> Bool {
        
        do {
            let regex = try NSRegularExpression(pattern: self.pattern, options: [])
            let matches = regex.matches(in: value, options: [], range: NSRange(location: 0, length: value.characters.count))
            if matches.count > 0 {
                return true
            }
        }
        catch {
            self.error = "something went wrong, please try again! if your problem doesn't solved after try, please restart the app"
            return false
        }
        self.error = "Wrong format, \(self.patternDescription!)"
        return false
    }
    
}
