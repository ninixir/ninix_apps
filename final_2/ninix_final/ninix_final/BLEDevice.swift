//
//  BLEDevice.swift
//  ninix_final
//
//  Created by Ali on 1/21/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth

class BLEDevice: NSObject {
    
    var localName: String
    var isConnectable: Bool
    var peripheral: CBPeripheral
    
    init(name: String, isConnectable: Bool, peripheral: CBPeripheral) {
        self.localName = name
        self.isConnectable = isConnectable
        self.peripheral = peripheral
    }
}
