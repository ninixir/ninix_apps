//
//  NXHealthStandard.swift
//  ninix_final
//
//  Created by Ali on 2/4/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class NXHealthStandard: NSObject {
    
    static let birthDate: Date? = NXUserDefault().retrieveBabyInformation().birthday
    
    static var onStomach: Int8 {
        return 1
    }
    
    static var thresholdTimeToSave: TimeInterval {
        return 30
    }
    
    static var temperatureTime: TimeInterval {
        return 60 * 10
    }
    
    static var respiratoryTime: TimeInterval {
        return 30
    }
    
    static var humidityTime: TimeInterval {
        return 60 * 5
    }
    
    static var orientationTime: TimeInterval {
        return 30
    }
    
    static var temperature: (up: Float, down: Float, high: Float, low: Float) {
        return (38, 36.5, 39, 35.5)
    }
    
    static var humidity: (up: Float, high: Float) {
        return (40, 80)
    }
    
    static var respiratory: (up: Int16, down: Int16, high: Int16, low: Int16) {
        if let date = self.birthDate {
            if NXDateFormatter(date: date).toYear() > 1 {
                return (38, 30, 45, 20)
            }
        }
        return (24, 22, 30, 20)
    }
}
