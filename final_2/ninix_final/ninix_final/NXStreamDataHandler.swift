//
//  NXStreamDataHandler.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift


class NXStreamDataHandler: InternetService, AuthenticationHeader {
    
    let sendAddress: String = NXWebServiceAddress.streamData
    var vitalSignController: NXVitalSignsController!
    var delegate: NXStreamDataHandlerDelegate?
    var dataPack: Results<NXVitalSignsModel>?
    var realm: Realm!
    var time: Date!
    
    func sync() {
        
        self.dataPack = NXVitalSignsController.didNotSync()
        guard let time: Date = dataPack?.sorted(byKeyPath: "time", ascending: false).first?.time as? Date else {
            return
        }
        self.time = time
        sessionManager.request(self.sendAddress, method: .post, parameters: self.toJson(dataPack!), encoding: JSONEncoding.default, headers: self.header).validate().responseObject(queue: self.queue) { (response: DataResponse<NXResponseJson>) in
            
            switch response.result {
                
            case .success:
                print(response.result.value ?? "nil", "response result value <NXSignupHandler:request>")
                self.validateResponse(response: response.result.value)
            case .failure(let error) :
                print(error, " error <NXSignupHandler:request>")
                self.delegate?.didReceiveError(error: error.localizedDescription)
                
            }
            
        }
    }
    
    func toJson(_ dataPack: Results<NXVitalSignsModel>) -> Parameters {
        var data: [Parameters] = []
        for singleData in dataPack {
            let jsonData : Parameters = [
                "temperature": singleData.temperature,
                "humidity": singleData.humidity,
                "respiratory": singleData.respiratory,
                "position": singleData.orientation,
                "timestamp": singleData.time.timeIntervalSince1970
            ]
            data.append(jsonData)
            
        }
        let parameters: Parameters = ["data": data]
        return parameters
    }
    
    func validateResponse(response: NXResponseJson?) {
        guard let response = response, let code = response.code else {
            delegate?.didReceiveServerError()
            return
        }
        
        if code == 0 {
            print("sucess commit <NXStreamDataHandler:validateResponse>")
            
            print("time is here")
            if (NXVitalSignsController.sync(time: time)) {
                delegate?.didUpdate()
            }
            else {
                delegate?.storageError()
            }
        }
        else {
            if let message = response.message_en {
                delegate?.didReceiveError(error: message)
            }
            else {
                delegate?.didReceiveServerError()
            }
        }
        
    }
    
}


protocol NXStreamDataHandlerDelegate: InternetServiceDelegate {
    func didUpdate()
    func storageError()
}
