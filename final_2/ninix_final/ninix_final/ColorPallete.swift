//
//  UIHelper.swift
//  ninix_final
//
//  Created by Ali on 1/9/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit


struct ColorPallete {
    
    static var primaryLight = UIColor(red: 151/255, green: 243/255, blue: 231/255, alpha: 1)
    static var primary = UIColor(red: 65/255, green: 234/255, blue: 212/255, alpha: 1)
    static var primaryDark = UIColor(red: 18/255, green: 64/255, blue: 58/255, alpha: 1)

    static var secondary = UIColor(red: 255/255, green: 104/255, blue: 107/255, alpha: 1)
    
    static var white = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    
    static var dark = UIColor(red: 7/255, green: 16/255, blue: 19/255, alpha: 1)
    
    static var helper = UIColor(red: 255/255, green: 219/255, blue: 15/255, alpha: 1)
    
    static var primaryGradient: CAGradientLayer {
        get {
            let gradient = CAGradientLayer()
            gradient.colors = [ColorPallete.primary.cgColor, ColorPallete.secondary.cgColor]
            gradient.locations = [0, 0.5]
            return gradient
        }
    }
}



