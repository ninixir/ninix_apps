//
//  SignupFormController.swift
//  ninix_final
//
//  Created by Ali on 12/31/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import Alamofire
import IBAnimatable
import Bond
import ReactiveKit
import PMAlertController
import KeychainSwift


class SignupFormController: FormController, NXSignupHandlerDelegate {
    
    
    @IBOutlet weak var mobileField: UITextField!
    @IBOutlet weak var fullNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var formHolder: UIStackView!
    let Validator = NXValidator()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        submitButton.setTitle("Register", for: UIControlState.normal)
        
        addFromController(placeHolder: formHolder)
        
        
        Validator.addFiled(textField: mobileField).setName(name: "mobile").addRule(rule: NoWhiteSpaceRule()).addRule(rule: SpecificCharSize(size: 11)).addRule(rule: RegexMatch(pattern: "\\b(09)(\\d{9})\\b", patternDescription: "Mobile number should start with 09 and should be 11 digits")).end()
        Validator.addFiled(textField: fullNameField).setName(name: "Full Name").addRule(rule: RangeCharSizeRule(min: 3, max: nil)).end()
        Validator.addFiled(textField: passwordField).setName(name: "password").addRule(rule: RangeCharSizeRule(min: 5, max: nil)).end()
        
    }
    
    
    func didRegistered() {
        DispatchQueue.main.async(){
            self.performSegue(withIdentifier: "success", sender: nil)
        }
    }
    
    func didReceiveServerError() {
        self.showError(decription: "Something went wrong in server, after few second please try again")
    }
    
    func didReceiveError(error: String) {
        self.showError(decription: error)
    }
    
    override func submit(_ sender: FancyButton) {

        if Validator.validate() {
            startRequest()
            print("no error <SignupFOrmController:submit>")
            NXSignupHandler(username: mobileField.text!, password: passwordField.text!, fullName: fullNameField.text!, delegate: self).request()
        }
        else {
            showError(decription: Validator.error ?? "something went wrong")
        }
    }
    
    @IBAction func gotoLogion(_ sender: AnyObject) {
        Router.routeToLogin()
    }
    
    
}
