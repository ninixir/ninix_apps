//
//  BabyRegisterController.swift
//  ninix_final
//
//  Created by Ali on 12/31/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit

class BabyRegisterController: FormController {
    
    let validator = NXValidator()
    @IBOutlet weak var babyNameTextField: UITextField!
    @IBOutlet weak var weightTetxField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var headTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        validator.addFiled(textField: babyNameTextField).setName(name: "Name").addRule(rule: RangeCharSizeRule(min: 3, max: nil)).end()
        validator.addFiled(textField: weightTetxField).setName(name: "Weight").addRule(rule: MustBeDigit()).end()
        validator.addFiled(textField: heightTextField).setName(name: "Height").addRule(rule: MustBeDigit()).end()
        validator.addFiled(textField: headTextField).setName(name: "Head Circumference").addRule(rule: MustBeDigit()).end()
        
    }
    
    override func submit(_ sender: FancyButton) {
        if validator.validate() {
            startRequest()
        }
        else {
            showError(decription: validator.error ?? "something went wrong")
        }
    }
    
    func didRecieveResponse(json: NSDictionary?) {
        
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "successSignUp", sender: nil)
        }
    }
    
    
    
    
    
    

}
