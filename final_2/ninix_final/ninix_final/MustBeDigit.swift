//
//  MustBeDigit.swift
//  ninix_final
//
//  Created by Ali on 2/6/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class MustBeDigit: ValidationRule {
    var error: String? = nil
    
    
    func validate(value: String) -> Bool {
        
        if let _ = Int(value) {
            return true
        }
        else {
            self.error = "Only Digit must be provided"
        }
        return false
        
    }
}
