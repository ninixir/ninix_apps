//
//  MainTbBarViewController.swift
//  ninix_final
//
//  Created by Ali on 10/18/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit

class MainTbBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = ColorPallete.white
        self.tabBar.barTintColor = ColorPallete.primaryDark
        self.tabBar.isTranslucent = false
        
        
        let unreadNotifications = NXNotificationController().unreadNotificationCount()
            print(unreadNotifications, "<MainTbBarViewController:viewDidLoad>")
            if unreadNotifications > 0 {
                self.tabBar.items?[2].badgeValue = "\(unreadNotifications)"
            }
            
        
        
    }

}
