//
//  RangeCharSizeRule.swift
//  ninix_final
//
//  Created by Ali on 2/6/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class RangeCharSizeRule: ValidationRule {
    var error: String? = nil
    let min: Int!
    let max: Int!
    
    init(min: Int?, max: Int?) {
        if let min = min {
            self.min = min
        }
        else {
            self.min = 0
        }
        if let max = max {
            self.max = max
        }
        else {
            self.max = Int.max
        }
        
    }
    
    func validate(value: String) -> Bool {
        
        let size = value.characters.count
        
        if size >= min && size <= max {
            return true
        }
        if size > max {
            self.error = "should be lower than \(max!) characters"
        }
        else {
            self.error = "should be more than \(min!) characters"
        }
        return false
    }
}
