//
//  PasswordSettingTableViewController.swift
//  ninix_final
//
//  Created by Ali on 1/3/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class PasswordSettingTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "User Settings"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "save", style: .done, target: self, action: #selector(UserSettingTableViewController.save(button:)))
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func save(button: UIBarButtonItem) {
        
    }

}
