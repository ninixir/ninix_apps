//
//  AnimateScrollView.swift
//  ninix_final
//
//  Created by Ali on 1/29/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import IBAnimatable

protocol AnimateScrollView {
    func animate(view: AnimatableScrollView)
}

extension AnimateScrollView {
    func animate(view: AnimatableScrollView) {
        view.pop(repeatCount: 1)
        view.squeeze(repeatCount: 1)
    }
}
