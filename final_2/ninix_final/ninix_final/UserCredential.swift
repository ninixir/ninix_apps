//
//  UserCredential.swift
//  ninix_final
//
//  Created by Ali on 1/31/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import KeychainSwift

struct UserCredential {
    static var username: String = "username"
    static var password: String = "password"
    static var token: String = "token"
    static var login: String = "login"
    
    static func save(username: String, password: String) {
        let keyChain = KeychainSwift()
        keyChain.set(username, forKey: self.username)
        keyChain.set(password, forKey: self.password)
    }
}
