//
//  SuccessRegisterController.swift
//  ninix_final
//
//  Created by Ali on 12/31/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import IBAnimatable

class SuccessRegisterController: UIViewController {

    @IBOutlet weak var image: AnimatableImageView!
    @IBOutlet weak var titleLabel: AnimatableLabel!
    @IBOutlet weak var subtitleLabel: AnimatableLabel!
    @IBOutlet weak var button: AnimatableButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.isHidden = true
        titleLabel.isHidden = true
        subtitleLabel.isHidden = true
        button.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        image.isHidden = false
        titleLabel.isHidden = false
        subtitleLabel.isHidden = false
        button.isHidden = false
        
        image.slideFade(.in, direction: .up)
        titleLabel.slideFade(.in, direction: .left)
        subtitleLabel.slideFade(.in, direction: .right)
        button.slideFade(.in, direction: .down)
    }
    

    @IBAction func gotoApp(_ sender: AnyObject) {
        Router.routeToMain()
    }

}
