//
//  NXNotificationModel.swift
//  ninix_final
//
//  Created by Ali on 1/31/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXNotificationModel: NXShouldSyncModel {
    
    dynamic var type: Int = 0
    dynamic var status: Int = 0
    dynamic var value: Float = 0.0
    dynamic var seen: Bool = false
    
}

enum NXNotificationType: Int {
    case temperature = 0, humidity, respiratory, orientation
    
    
}

enum NXNotificationStatus: Int {
    case normal = 0, warning, danger
    
    
}
