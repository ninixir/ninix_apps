//
//  AddDeviceTableControllerTableViewController.swift
//  ninix
//
//  Created by Ali on 10/8/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import CoreBluetooth

class AddDeviceTableControllerTableViewController: UITableViewController, CBCentralManagerDelegate{
    
    var manager:CBCentralManager!
    var peripheral:CBPeripheral!
    var devices : [NSString] = []
    var peripherals : [CBPeripheral] = []
    var device : String? = nil
    

    override func viewDidLoad() {
        super.viewDidLoad()
        manager = CBCentralManager(delegate: self, queue: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return devices.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "devicecell", for: indexPath)
        cell.textLabel?.text = devices[indexPath.row] as String
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        peripheral = peripherals[indexPath.row]
        self.device = devices[indexPath.row] as String
        self.manager.connect(self.peripheral, options: nil)
        
        var overlay : UIView!
        var progressLabel : UILabel!
        
        overlay = UIView(frame: view.frame)
        overlay.backgroundColor = UIColor.black
        overlay.alpha = 0.5
        
        progressLabel = UILabel()
        progressLabel.text = "Connecting..."
        progressLabel.textColor = UIColor.white
        progressLabel.textAlignment = .center
        progressLabel.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(overlay)
        view.addSubview(progressLabel)
        
        let widthConstraint = NSLayoutConstraint(item: progressLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 250)
        let heightConstraint = NSLayoutConstraint(item: progressLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100)
        let xConstraint = NSLayoutConstraint(item: progressLabel, attribute: .centerX, relatedBy: .equal, toItem: self.tableView, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: progressLabel, attribute: .centerY, relatedBy: .equal, toItem: self.tableView, attribute: .centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([widthConstraint, heightConstraint, xConstraint, yConstraint])
        
    }
    
    // MARK: - Scan bluetooth devices
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        if central.state == .poweredOn {
            central.scanForPeripherals(withServices: [Ninix_UUID.SERVICE_CBUUID], options: nil)
        }
        
    }
    
    
    // MARK: - Show connected Devices
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let dictionary = advertisementData as NSDictionary
        if let device =  (dictionary.object(forKey: CBAdvertisementDataLocalNameKey) as? NSString) {
            
            if !devices.contains(device) {
                devices.append(device)
                peripherals.append(peripheral)
            }
            self.tableView.reloadData()
        }
        
        
    }
    
    // MARK: - Connect to selected devices
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        peripheral.discoverServices([Ninix_UUID.SERVICE_CBUUID])
        manager.stopScan()
        performSegue(withIdentifier: "connectedDevice", sender: self)
        
    }
    
    
    
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
