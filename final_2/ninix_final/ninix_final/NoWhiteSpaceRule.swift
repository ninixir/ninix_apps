//
//  NoWhiteSpaceRule.swift
//  ninix_final
//
//  Created by Ali on 2/6/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class NoWhiteSpaceRule: ValidationRule {
    
    var error: String? = nil
    
    func validate(value: String) -> Bool {
        if value.range(of: " ") == nil {
            return true
        }
        self.error = "cannot contain white space"
        return false
    }
}
