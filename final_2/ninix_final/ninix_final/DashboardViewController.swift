//
//  DashboardViewController.swift
//  ninix_final
//
//  Created by Ali on 1/28/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import CoreBluetooth

class DashboardViewController: ThemedUIViewController, NXPeripheralDelegate {

    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var respiratoryLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var centerpicture: UIImageView!
    var type: NXNotificationType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NXPeripheral.getInstance().delegate = self
        
        centerpicture.layer.cornerRadius = centerpicture.frame.size.width/2
        centerpicture.clipsToBounds = true
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func peripheral(didUpdateValue value: NXBluetoothData) {
        DispatchQueue.main.async {
            if value.humidity > 50 {
                self.humidityLabel.text = "Pooped"
            }
            else {
                self.humidityLabel.text = "Normal"
            }
            
            if value.orientation == 1 {
                self.positionLabel.text = "On Back"
            }
            else {
                self.positionLabel.text = "On Stomach"
            }
            self.temperatureLabel.text = String(format: "%.1f °C", value.temperature)
            self.respiratoryLabel.text = "\(value.respiratory!) Bps"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue, segue.identifier, segue.destination)
        if segue.identifier == "chart" {
            guard let chartViewController = segue.destination as? ChartViewController else {
                return
            }
            chartViewController.type = self.type
        }
    }
    
    @IBAction func showChartTemperature(_ sender: UITapGestureRecognizer) {
        self.type = .temperature
        performSegue(withIdentifier: "chart", sender: nil)
    }
    
    @IBAction func showChartRespiratory(_ sender: Any) {
        self.type = .respiratory
        performSegue(withIdentifier: "chart", sender: nil)
    }
    
    
    
    
}
