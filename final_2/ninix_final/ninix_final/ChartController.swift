//
//  ChartController.swift
//  ninix_final
//
//  Created by Ali on 10/23/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import Foundation

protocol ChartController {
    
    func ChangeType(type: ChartType)
    
}
