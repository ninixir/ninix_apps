//
//  TextFieldFirstResponder.swift
//  ninix_final
//
//  Created by Ali on 1/22/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class TextFieldFirstResponder {
    
    public static func whoIsResponder(current: UITextField, textFields: [UITextField]) {
        current.resignFirstResponder()
        if textFields.contains(current) {
            if let index = textFields.index(of: current) {
                if index < textFields.count - 1 {
                    textFields[index + 1].becomeFirstResponder()
                    print(textFields[index + 1].canBecomeFirstResponder, "first responder")
                }
            }
        }
    }
    
}

