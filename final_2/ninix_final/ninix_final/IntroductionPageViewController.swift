//
//  IntroductionPageViewController.swift
//  ninix_final
//
//  Created by Ali on 12/28/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit

class IntroductionPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var introductionPages: [UIViewController]!
    var pageController: UIPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        introductionPages = [
            getIntroductionPage(identifier: "first"),
            getIntroductionPage(identifier: "second"),
            getIntroductionPage(identifier: "third"),
            getIntroductionPage(identifier: "fourth")]
        
        setViewControllers([introductionPages.first!], direction: .forward, animated: true, completion: nil)
        
        let frame = CGRect(x: CGFloat(0), y: view.frame.height - 60.0, width: view.frame.width, height: CGFloat(20))
        pageController = UIPageControl(frame: frame)
        pageController.backgroundColor = UIColor.clear
        pageController.numberOfPages = introductionPages.count
        pageController.currentPage = 0
        pageController.pageIndicatorTintColor = ColorPallete.primary
        pageController.currentPageIndicatorTintColor = ColorPallete.secondary
        view.addSubview(pageController)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            let viewControllers = pageViewController.viewControllers!
            if let index = introductionPages.index(of: viewControllers.first!) {
                pageController.currentPage = index
            }
        }
    }
    
    
    /*
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
    }
    
    
    // Sent when a gesture-initiated transition ends. The 'finished' parameter indicates whether the animation finished, while the 'completed' parameter indicates whether the transition completed or bailed out (if the user let go early).
    
    
    
    // Delegate may specify a different spine location for after the interface orientation change. Only sent for transition style 'UIPageViewControllerTransitionStylePageCurl'.
    // Delegate may set new view controllers or update double-sided state within this method's implementation as well.
    func pageViewController(_ pageViewController: UIPageViewController, spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewControllerSpineLocation {
        
    }
    
    
    func pageViewControllerSupportedInterfaceOrientations(_ pageViewController: UIPageViewController) -> UIInterfaceOrientationMask {
        
    }
    
    func pageViewControllerPreferredInterfaceOrientationForPresentation(_ pageViewController: UIPageViewController) -> UIInterfaceOrientation {
        
    }
 */
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let index = introductionPages.index(of: viewController) {
            
            if index - 1 > -1 {
                return introductionPages[index - 1]
            }
            else {
                return nil
            }
        }
        return nil
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let index = introductionPages.index(of: viewController) {
            
            if index + 1 > 3 {
                return nil
            }
            else {
                return introductionPages[index + 1]
            }
        }
        return nil
    }
 
    
    func getIntroductionPage(identifier: String) -> UIViewController {
        let viewController = UIStoryboard(name: "Introduction", bundle: nil).instantiateViewController(withIdentifier: identifier)
        return viewController
    }

    

}
