//
//  NXTokenHandler.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import KeychainSwift

class NXTokenHandler {
    var tokenType: String
    var accessToken: String
    let keyChain: KeychainSwift = KeychainSwift()
    
    
    init(tokenType: String, accessToken: String) {
        self.tokenType = tokenType
        self.accessToken = accessToken
    }
    
    init?(jsonToken: NXTokenJson) {
        
        if let tokenType = jsonToken.token_type, let accessToken = jsonToken.access_token {
            self.tokenType = tokenType
            self.accessToken = accessToken
        }
        else {
            return nil
        }
        
    }
    
    func save() {
        let token = "\(tokenType) \(accessToken)"
        keyChain.set(token, forKey: UserCredential.token)
        print("token saved <NXTokenHandler:save>", token)
    }
    
    static func get() -> String? {
        let keyChain: KeychainSwift = KeychainSwift()
        return keyChain.get(UserCredential.token)
    }
}
