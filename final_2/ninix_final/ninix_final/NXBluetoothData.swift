//
//  NXBluetoothData.swift
//  ninix_final
//
//  Created by Ali on 2/6/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class NXBluetoothData: NSObject {
    var temperature: Float!
    var humidity: Float!
    var respiratory: Int16!
    var orientation: Int8!
    
    init(temperature: Float, humidity: Float, respiratory: Int16, orientation: Int8) {
        self.temperature = NXHelper.round(temperature, toNearest: 0.1)
        self.humidity = NXHelper.round(humidity, toNearest: 0.1)
        self.respiratory = respiratory
        self.orientation = orientation
    }
    
    func toNXVitalSignsModel() -> NXVitalSignsModel {
        let vitalSigns = NXVitalSignsModel()
        vitalSigns.temperature = self.temperature
        vitalSigns.humidity = self.humidity
        vitalSigns.respiratory = Int(self.respiratory)
        vitalSigns.orientation = Int(self.orientation)
        
        
        return vitalSigns
    }
}

struct NXLastReceivedData {
    static var data: NXBluetoothData? = nil
    static var time: Date? = nil
}
