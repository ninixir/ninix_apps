//
//  ShowPopupMessage.swift
//  ninix_final
//
//  Created by Ali on 2/2/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import PMAlertController

protocol ShowPopupMessage {
    func showError(title: String, decription: String, image: UIImage?, style: PMAlertControllerStyle, actionTitle: String, viewController: UIViewController)
}


extension ShowPopupMessage {
    func showError(title: String = "Error", decription: String, image: UIImage? = nil, style: PMAlertControllerStyle = .alert, actionTitle: String = "ok", viewController: UIViewController) {
        let alertVC = PMAlertController(title: title, description: decription, image: image, style: style)
        
        alertVC.addAction(PMAlertAction(title: actionTitle, style: .cancel, action: nil))
        DispatchQueue.main.async {
            viewController.view.endEditing(true)
            viewController.present(alertVC, animated: true, completion: nil)
        }
        
        
    }
}
