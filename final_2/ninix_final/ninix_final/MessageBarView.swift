//
//  MessageBarView.swift
//  ninix_final
//
//  Created by Ali on 1/21/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import Dodo

class MessageBarView {
    
    let appDelegate: AppDelegate!
    
    init() {
        appDelegate = UIApplication.shared.delegate as! AppDelegate
    }
    
    private func adjust() -> UIViewController? {
        if let root = self.appDelegate.window?.rootViewController as? UITabBarController {
            root.view.dodo.style.leftButton.icon = .close
            root.view.dodo.style.leftButton.onTap = {
                root.view.dodo.hide()
            }
            root.view.dodo.topLayoutGuide = root.topLayoutGuide
            root.view.dodo.bottomLayoutGuide = root.bottomLayoutGuide
            return root
        }
        
        return nil
    }
    
    func showWarning(message: String) {
        
        if let root = self.adjust() {
            root.view.dodo.hide()
            root.view.dodo.error(message)
        }
        else {
            print("not in main storyboard")
        }
    }
    
    func hide() {
        if let root = self.appDelegate.window?.rootViewController as? UITabBarController {
            root.view.dodo.hide()
        }
    }
    
}
