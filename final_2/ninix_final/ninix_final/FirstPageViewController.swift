//
//  FirstPageViewController.swift
//  ninix_final
//
//  Created by Ali on 12/28/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import IBAnimatable

class FirstPageViewController: UIViewController {

    
    @IBOutlet weak var image: AnimatableImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        image.pop(repeatCount: 1)
        
    }
    

}
