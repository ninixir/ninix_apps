//
//  ConnectSuccessfullyViewController.swift
//  ninix_final
//
//  Created by Ali on 12/31/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import IBAnimatable

class ConnectSuccessfullyViewController: UIViewController {
    
    @IBOutlet weak var continueLabel: UIButton!
    @IBOutlet weak var image: AnimatableImageView!
    
    @IBOutlet weak var titleLabel: AnimatableLabel!
    @IBOutlet weak var textLabel: AnimatableLabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if AppStateService.didAppIntroduced() {
            continueLabel.setTitle("Continue to the App", for: .normal)
        }
        else {
            continueLabel.setTitle("Next Step, Login or Register", for: .normal)
        }
        image.isHidden = true
        textLabel.isHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        image.isHidden = false
        textLabel.isHidden = false
        image.slide(.in, direction: .up)
        titleLabel.pop(repeatCount: 1)
        textLabel.slideFade(.in, direction: .down)
    }
    

    @IBAction func goToSignUp(_ sender: AnyObject) {
        
        Router.routeToMain()
        /*
        if AppStateService.didAppIntroduced() {
            if AppStateService.didUserLoggedIn() {
                Router.routeToMain()
            }
            else {
                Router.routeToLogin()
            }
        }
        else {
            Router.routeToSignup()
        }
    */
    }
    

}
