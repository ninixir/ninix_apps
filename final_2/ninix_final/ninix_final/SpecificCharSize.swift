//
//  SpecificCharSize.swift
//  ninix_final
//
//  Created by Ali on 2/6/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class SpecificCharSize: ValidationRule {
    var error: String? = nil
    let size: Int!
    
    
    init(size: Int) {
        self.size = size
        
    }
    
    func validate(value: String) -> Bool {
        
        let valueSize = value.characters.count
        
        if valueSize == size {
            return true
        }
        
        self.error = "must be \(size!) characters"
        return false
    }
}
