//
//  NXCentralManagerDelegate.swift
//  ninix_final
//
//  Created by Ali on 1/21/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc protocol NXCentralManagerDelegate {
    
    @objc optional func nxCentralManager(_ central: CBCentralManager, didDiscover device: BLEDevice)
    func nxCentralManagerDidUpdateState(_ central: CBCentralManager)
    func nxCentralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral)
}
