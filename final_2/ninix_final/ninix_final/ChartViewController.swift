//
//  ChartViewController.swift
//  ninix_final
//
//  Created by Ali on 2/15/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import Charts

class ChartViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    var type: NXNotificationType!
    
    var lineChartView: LineChartView!
    var barChartView: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = type == .temperature ? "Temperature Charts" : "Respiratory Charts"
        let height = CGFloat(70.0)
        print(view.frame.size.width, view.frame.size.height / 2, "width height")
        self.lineChartView = LineChartView(frame: CGRect(x: 0, y: height, width: view.frame.size.width, height: (view.frame.size.height - height) / 2))
        self.barChartView = BarChartView(frame: CGRect(x: 0, y: height + (view.frame.size.height - height) / 2 , width: view.frame.size.width, height: (view.frame.size.height - height) / 2))
        view.addSubview(self.lineChartView)
        view.addSubview(barChartView)
        self.plotChart()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismiss(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func plotChart() {
        var dataEntries: [ChartDataEntry] = []
        var dataEntriesBar: [BarChartDataEntry] = []
        for i in 0...10 {
            let chartDataEntry = ChartDataEntry(x: Double(i), y: Double(i) * 0.345)
            let barChartDataEntry = BarChartDataEntry(x: Double(i), y: Double(i) * 0.345)
            dataEntries.append(chartDataEntry)
            dataEntriesBar.append(barChartDataEntry)
        }
        
        let dataSet = LineChartDataSet(values: dataEntries, label: "Temperature")
        dataSet.mode = .horizontalBezier
        dataSet.fillColor = UIColor.red
        dataSet.setColors(UIColor.orange, UIColor.yellow)
        dataSet.drawFilledEnabled = true
        let chartData = LineChartData(dataSet: dataSet)
        
        //lineChartView.getAxis(.left).gridColor = UIColor.white
        lineChartView.getAxis(.left).drawGridLinesEnabled = false
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.enabled = false
        lineChartView.getAxis(.right).enabled = false
        
        
        
        lineChartView.data = chartData
        lineChartView.noDataText = "there is no data"
        
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.xAxis.labelPosition = .bottom
        barChartView.getAxis(.left).enabled = false
        barChartView.getAxis(.right).enabled = false
        
        let dataSetBar = BarChartDataSet(values: dataEntriesBar, label: "Temperature")
        let chartDataBar = BarChartData(dataSet: dataSetBar)
        barChartView.data = chartDataBar
        
    }
    
    
    

}
