//
//  CircleView.swift
//  ninix_final
//
//  Created by Ali on 1/15/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class CircleView: UIView {

    
    override func draw(_ rect: CGRect) {
        
        self.layer.borderWidth = 3
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.cornerRadius = self.frame.size.width/2
        
        
    }
 

}
