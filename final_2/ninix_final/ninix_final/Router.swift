//
//  Router.swift
//  ninix_final
//
//  Created by Ali on 1/8/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class Router {
    
    static var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    private static func routeTo(storyBoard: String, viewController: String) {
        self.appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let rootViewController = UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: viewController)
        
        appDelegate.window?.rootViewController = rootViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    static func routeToIntroduction() {
        self.routeTo(storyBoard: NXStoryBoard.introduction, viewController: NXViewControllerIdentifier.firstIntroductionPage)
    }
    
    static func routeToConnect() {
        self.routeTo(storyBoard: NXStoryBoard.connect, viewController: NXViewControllerIdentifier.discoveredDevicesList)
    }
    
    static func routeToLogin() {
        self.routeTo(storyBoard: NXStoryBoard.login, viewController: NXViewControllerIdentifier.loginForm)
    }
    
    static func routeToSignup() {
        // TODO: correct routing
        /*
        if NXPeripheral.getInstance()._UUID == nil {
            Router.routeToConnect()
            return
        }
        */
        self.routeTo(storyBoard: NXStoryBoard.signup, viewController: NXViewControllerIdentifier.singupForm)
    }
    
    static func routeToMain() {
        //self.routeTo(storyBoard: NXStoryBoard.main, viewController: NXViewControllerIdentifier.appMainTabBar)
        
        if AppStateService.didUserLoggedIn() {
            self.routeTo(storyBoard: NXStoryBoard.main, viewController: NXViewControllerIdentifier.appMainTabBar)
        }
        else {
            self.routeToLogin()
        }
        
    }
}

struct NXStoryBoard {
    static let introduction = "Introduction"
    static let connect = "Connect"
    static let login = "Login"
    static let signup = "Signup"
    static let main = "Main"
}

struct NXViewControllerIdentifier {
    static let firstIntroductionPage = "introduction"
    static let discoveredDevicesList = "connect"
    static let loginForm = "login"
    static let singupForm = "signup"
    static let appMainTabBar = "main"
}



