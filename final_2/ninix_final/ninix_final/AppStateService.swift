//
//  AppStateService.swift
//  ninix_final
//
//  Created by Ali on 1/8/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import KeychainSwift

class AppStateService {
    static var defaults = UserDefaults.standard
    
    
    static func didAppIntroduced() -> Bool {
        return self.defaults.bool(forKey: DefaultKeys.didIntroduceApp.rawValue)
    }
    
    static func didUserLoggedIn() -> Bool {
        if NXTokenHandler.get() == nil {
            return false
        }
        let keyChain = KeychainSwift()
        return keyChain.getBool(UserCredential.login) ?? false
    }
    
    static func appIntroduced() {
        self.defaults.set(true, forKey: DefaultKeys.didIntroduceApp.rawValue)
        self.defaults.synchronize()
    }
    
    static func userLoggedIn() {
        
        let keyChain = KeychainSwift()
        keyChain.set(true, forKey: UserCredential.login)
    }
    
    static func userLoggedOut() {
        let keyChain = KeychainSwift()
        keyChain.set(false, forKey: UserCredential.login)
    }
}
