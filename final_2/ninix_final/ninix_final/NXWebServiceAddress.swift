//
//  NXWebServiceAddress.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

struct NXWebServiceAddress {
    
    static let mainAddress: String = "https://api.ninixco.com/"
    
    static var registerUser: String {
        get {
            return self.mainAddress.appending("user/register")
        }
    }
    
    static var registerBaby: String {
        get {
            return self.mainAddress.appending("baby/sync")
        }
    }
    
    static var login: String {
        get {
            return self.mainAddress.appending("user/login")
        }
    }
    
    static var syncUser: String {
        get {
            return self.mainAddress.appending("user/sync")
        }
    }
    
    static var syncBaby: String {
        get {
            return self.registerBaby
        }
    }
    
    static var streamData: String {
        get {
            return self.mainAddress.appending("stream/send")
        }
    }
    
    static var syncNotification: String {
        get {
            return self.mainAddress.appending("notification/sync")
        }
    }
    
}
