//
//  NXWebServiceSetting.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

struct NXWebServiceSettings {
    static let timeout: TimeInterval = 10
}
