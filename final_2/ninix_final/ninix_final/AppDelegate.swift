//
//  AppDelegate.swift
//  ninix_final
//
//  Created by Ali on 10/16/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import Dodo
import CoreBluetooth
import KeychainSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if let tehranGMT = TimeZone(secondsFromGMT: Int(3600 * 3.5)) {
            NSTimeZone.default = tehranGMT
        }
        
        
        
        
        // TODO: User UNNotificationSettings instead of UIUserNotificationSettings
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                print(granted, " granted <APPdelegate:application>")
            }
        } else {
            // Fallback on earlier versions
            application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
        }
        
        
        let _ = ReachabilityService.getInstance()
        
        print("didFinishLaunchingWithOptions <AppDelegate:application>")
        
        
        if AppStateService.didAppIntroduced() {
            print("App Introduced <AppDelegate:application>")
            if AppStateService.didUserLoggedIn() {
                print("user logged in <AppDelegate:application>")
                Router.routeToMain()
            }
            else {
                print("user not logged in <AppDelegate:application>")
                Router.routeToLogin()
            }
        }
        else {
            print("app didn't introduced <AppDelegate:application>")
            Router.routeToIntroduction()
        }
        
        Router.routeToMain()
        //Router.routeToIntroduction()
        //Router.routeToLogin()
        //Router.routeToSignup()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        let unreadNotif = NXNotificationController().unreadNotificationCount()
        UIApplication.shared.applicationIconBadgeNumber = unreadNotif
        
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if NXPeripheral.getInstance().peripheral?.state != .connected {
            print("no bluetooth connection <AppDelegate:applicationDidBecomeActive>")
            MessageBarView().showWarning(message: "Error: No Bluetooth connection")
        }
        else {
            print("bluetooth is connect <AppDelegate:applicationDidBecomeActive>")
            MessageBarView().hide()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    


}

