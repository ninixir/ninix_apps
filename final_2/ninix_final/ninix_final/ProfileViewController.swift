//
//  ProfileViewController.swift
//  ninix
//
//  Created by Ali on 10/7/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import Fusuma

class ProfileViewController: ThemedUIViewController, FusumaDelegate {
    
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var babyPicture: UIImageView!
    
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userAgeLabel: UILabel!
    @IBOutlet weak var userChildrenLabel: UILabel!
    @IBOutlet weak var realtionLabel: UILabel!
    @IBOutlet weak var userAddressLabel: UILabel!
    
    @IBOutlet weak var babyFullNameLabel: UILabel!
    @IBOutlet weak var babyAgeLabel: UILabel!
    @IBOutlet weak var babyWeightLabel: UILabel!
    @IBOutlet weak var babyHeightLabel: UILabel!
    @IBOutlet weak var babyHeadLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nxDefaults = NXUserDefault()
        let userInformation = nxDefaults.retrieveUserInformation()
        let babayInformation = nxDefaults.retrieveBabyInformation()
        
        if let date = userInformation.birthday {
            userAgeLabel.text = String(NXDateFormatter(date: date).toYear())
        }
        else {
            userAgeLabel.text = "Not set"
        }
        
        let children = userInformation.children
        let relation = userInformation.relation?.rawValue
        
        userFullNameLabel.text = userInformation.fullName ?? "Not set"
        
        userChildrenLabel.text = children != nil ? "\(children!)" : "Not set"
        realtionLabel.text = relation == 0 ? "Father" : relation == 1 ? "Mother" : relation == 2 ? "Other" : "Not set"
        userAddressLabel.text = userInformation.address ?? "Not set"
        
        let weight = babayInformation.weight
        let height = babayInformation.height
        let head = babayInformation.headCircumference
        
        if let date = babayInformation.birthday {
            babyAgeLabel.text = String(NXDateFormatter(date: date).toDay())
        }
        else {
            babyAgeLabel.text = "Not set"
        }
        
        babyFullNameLabel.text = babayInformation.name ?? "Not Set"
        babyWeightLabel.text = weight != nil ? "\(weight!)" : "Not set"
        babyHeightLabel.text = height != nil ? "\(height!)" : "Not set"
        babyHeadLabel.text = head != nil ? "\(head!)" : "Not set"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
        
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profilePicture.layer.cornerRadius = profilePicture.frame.size.width/2
        profilePicture.clipsToBounds = true
        
        babyPicture.layer.cornerRadius = babyPicture.frame.size.width/2
        babyPicture.clipsToBounds = true
    }
    
    @IBAction func userProfilePictureTapped(_ sender: AnyObject) {
        
        let fusuma = FusumaViewController()
        fusuma.hasVideo = false
        fusuma.delegate = self
        fusumaCropImage = true
        present(fusuma, animated: true, completion: nil)
        print("tapped me")
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        profilePicture.image = image
        print("Image selected")
    }
    
    // Return the image but called after is dismissed.
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
        print("Called just after FusumaViewController is dismissed.")
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        print("Called just after a video has been selected.")
    }
    
    // When camera roll is not authorized, this method is called.
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
    }
    
    func fusumaClosed() {
        print("closed")
    }

}
