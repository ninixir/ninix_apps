//
//  LoginFormController.swift
//  ninix_final
//
//  Created by Ali on 1/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import Alamofire
import PMAlertController

class LoginFormController: FormController, NXLoginHandlerDelegate {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var formHolder: UIStackView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        submitButton.setTitle("Login", for: UIControlState.normal)
        self.addFromController(placeHolder: formHolder)
    }
    
    func didLogin() {
        DispatchQueue.main.async {
            Router.routeToMain()
        }
    }
    
    func didReceiveError(error: String) {
        showError(decription: error)
    }
    
    func didReceiveServerError() {
        
        showError(decription: "something went wrong on server, after few seconds please try again")
    }
    
    override func submit(_ sender: FancyButton) {
        startRequest()
        NXLoginHandler(username: usernameField.text!, password: passwordField.text!, delegate: self).request()
    }
    
    @IBAction func signup(_ sender: Any) {
        Router.routeToSignup()
    }
    
    
    
    

}
