//
//  NXShouldSyncModel.swift
//  ninix_final
//
//  Created by Ali on 2/12/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXShouldSyncModel: Object {
    dynamic var didSync: Bool = false
    dynamic var time = NSDate()
}
