//
//  NXSyncNotificationHandler.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class NXSyncNotificationHandler: InternetService, AuthenticationHeader {
    
    let address: String = NXWebServiceAddress.syncNotification
    var delegate: NXSyncNotificationHandlerDelegate?
    var time: Date?
    
    func sync() {
        
        
        let notifications = NXNotificationController().all()
        self.time = notifications.last?.time as Date?
        
        sessionManager.request(self.address, method: .post, parameters: self.toJson(notifications), encoding: JSONEncoding.default, headers: self.header).validate().responseObject(queue: self.queue) { (response: DataResponse<NXResponseJson>) in
            
            switch response.result {
                
            case .success:
                print(response.result.value ?? "nil", "response result value <NXSyncNotificationHandler:request>")
                self.validateResponse(response: response.result.value)
            case .failure(let error) :
                print(error, " error <NXSyncNotificationHandler:request>")
                self.delegate?.didReceiveError(error: error.localizedDescription)
                
            }
            
        }
    }
    
    
    func toJson(_ notifications: Results<NXNotificationModel>) -> Parameters {
        
        var data: [Parameters] = []
        for notification in notifications {
            let jsonNotification : Parameters = [
                "type": notification.type,
                "status": notification.type,
                "value": notification.value,
                "time": notification.time.timeIntervalSince1970,
                "seen": notification.seen
            ]
            data.append(jsonNotification)
            
        }
        let parameters: Parameters = ["notifications": data]
        print(parameters, "parameteres <NXSyncNotificationHandler:toJson>")
        return parameters
    }
    
    func validateResponse(response: NXResponseJson?) {
        guard let response = response, let code = response.code else {
            delegate?.didReceiveServerError()
            return
        }
        
        if code == 0 {
            
            if let time = self.time {
                let _ = NXNotificationController.sync(time: time)
            }

        }
            
        else {
            if let message = response.message_en {
                delegate?.didReceiveError(error: message)
            }
            else {
                delegate?.didReceiveServerError()
            }
        }
        
    }
    
}


protocol NXSyncNotificationHandlerDelegate: InternetServiceDelegate {
    func didUpdate()
    func storageError()
}
