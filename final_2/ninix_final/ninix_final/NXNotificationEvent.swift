//
//  NXNotificationEvent.swift
//  ninix_final
//
//  Created by Ali on 2/14/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class NXNotificationEvent {
    
    init(notificationModel: NXNotificationModel) {
        NXSyncNotificationHandler().sync()
        guard let type = toString(type: NXNotificationType(rawValue: notificationModel.type)) else {
            return
        }
        
        guard let status = toString(status: NXNotificationStatus(rawValue: notificationModel.status)) else {
            return
        }
        
        let title = "\(type) | \(status)"
        let subtitle = "check your baby"
        let badges = NXNotificationController().unreadNotificationCount()
        if #available(iOS 10.0, *) {
            NXNotificationLocal(title: title, subtitle: subtitle, badge: NSNumber(integerLiteral: badges)).send()
        } else {
            // Fallback on earlier versions
        }
    }
    
    func toString(type: NXNotificationType?) -> String? {
        
        guard let type = type else {
            return nil
        }
        
        switch type {
        case .temperature:
            return "Temperature"
        case .humidity:
            return "Humidity"
        case .respiratory:
            return "Respiratory"
        case .orientation:
            return "Orientation"
            
        }
    }
    
    func toString(status: NXNotificationStatus?) -> String? {
        
        guard let status = status else {
            return nil
        }
        
        switch status {
        case .normal:
            return "Normal"
        case .warning:
            return "Warning"
        case .danger:
            return "Danger"
            
        }
    }
}
