//
//  BabyInformation.swift
//  ninix_final
//
//  Created by Ali on 1/31/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class BabyInformation: NSObject, NSCoding {
    var name: String?
    var birthday: Date?
    var weight: Int?
    var height: Int?
    var headCircumference: Int?
    var gender: Gender?
    
    override init() {
        super.init()
    }
    
    init(name: String?, birthday: Date?, weight: Int?, height: Int?, headCircumference: Int?, gender: Gender?) {
        self.name = name
        self.birthday = birthday
        self.weight = weight
        self.height = height
        self.headCircumference = headCircumference
        self.gender = gender
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as? String
        let birthday = aDecoder.decodeObject(forKey: "babyBirthday") as? Date
        let weight = aDecoder.decodeObject(forKey: "weight") as? Int
        let height = aDecoder.decodeObject(forKey: "height") as? Int
        let headCircumference = aDecoder.decodeObject(forKey: "headCircumference") as? Int
        var gender: Gender? = nil
        if let genderInt = aDecoder.decodeObject(forKey: "gender") as? Int {
            gender = Gender(rawValue: genderInt)
        }
        self.init(name: name, birthday: birthday, weight: weight, height: height, headCircumference: headCircumference, gender: gender)
    }
    
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(birthday, forKey: "babyBirthday")
        aCoder.encode(weight, forKey: "weight")
        aCoder.encode(height, forKey: "height")
        aCoder.encode(headCircumference, forKey: "headCircumference")
        aCoder.encode(gender?.rawValue, forKey: "gender")
        
    }
    
    
    func getInformation() -> [Int: (String, String)] {
        var age: Int? = nil
        if let birthday = self.birthday {
            age = -Int(birthday.timeIntervalSinceNow) / (3600 * 24)
        }
        let name = self.name != nil ? self.name! : "-"
        let birthday = age != nil ? "\(age!) days" : "-"
        let weight = self.weight != nil ? "\(self.weight!) gr" : "-"
        let height = self.height != nil ? "\(self.height!) cm" : "-"
        let headCircumference = self.headCircumference != nil ? "\(self.headCircumference!) cm" : "-"
        let gender = self.gender != nil ? "\(self.gender!.rawValue)" : "-"
        
        return [0: ("Name", name), 1: ("Gender", gender), 2: ("Age", birthday), 3: ("Weight", weight), 4: ("Height", height), 5: ("Head Circumference", headCircumference)]
    }
}
