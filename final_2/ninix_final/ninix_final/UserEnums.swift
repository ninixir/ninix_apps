//
//  UserEnums.swift
//  ninix_final
//
//  Created by Ali on 1/31/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

enum Relation: Int {
    case father = 0
    case mother = 1
    case other = 2
    
}

enum Gender: Int {
    case boy = 0
    case girl = 1
    
}

enum DefaultKeys: String {
    case userCredential
    case userInformation
    case babyInformation
    case didIntroduceApp
    case loggedIn
    case token
}
