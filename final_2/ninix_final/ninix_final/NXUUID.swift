//
//  Bluetooth.swift
//  ninix
//
//  Created by Ali on 10/9/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import CoreBluetooth

struct NXUUID {
    
    static let mainService = CBUUID(string: "00001523-0000-1000-8000-00805f9b34fb")
    static let mainCharacteristic = CBUUID(string: "00001524-0000-1000-8000-00805f9b34fb")
    
    static var services: [CBUUID] {
        get {
            return [self.mainService]
        }
    }
    
    static var characteristics: [CBUUID] {
        get {
            return [self.mainCharacteristic]
        }
    }
    
}






