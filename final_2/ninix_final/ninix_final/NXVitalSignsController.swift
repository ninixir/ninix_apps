//
//  NXVitalSignsHandler.swift
//  ninix_final
//
//  Created by Ali on 1/31/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXVitalSignsController: NXModelController<NXVitalSignsModel>, SyncAbleModel {
    
    typealias Model = NXVitalSignsModel

 
    
    func save(object: NXVitalSignsModel) {
        
        
        if let last = last {
            if object == last {
                return
            }
            
        }
        NXHealthChecker(data: object.toBluetoothData()).analyse()
        super.save(object: object)
        
        
    }
    
    func save(object: NXVitalSignsModel, force: Bool) {
        
        
        
        if let last = last {
            if object == last, !force {
                return
            }
            if object === last {
                return
            }
            
        }
        NXHealthChecker(data: object.toBluetoothData()).analyse()
        super.save(object: object)
    }
    
    func average(date: NSDate, property: String) -> Double? {
        
        let objects = realm.objects(NXVitalSignsModel.self).filter(" time >= %@", date)
        var total: Double = 0
        var count: Double = 0
        
        for (index, object) in objects.enumerated() {
            var timeInterval = index < objects.count - 1 ? (objects[index + 1].time.timeIntervalSince1970 - object.time.timeIntervalSince1970) : (Date().timeIntervalSince1970 - object.time.timeIntervalSince1970)
            timeInterval = timeInterval > NXHealthStandard.thresholdTimeToSave ? 1 : NXHealthStandard.thresholdTimeToSave
            guard let value = object.value(forKey: property) as? NSNumber else {
                return nil
            }
            total += Double(value) * timeInterval
            count += timeInterval
        }
        
        if count == 0 {
            return nil
        }
        
        return total / Double(count)
    }
    
    
    
    
}
