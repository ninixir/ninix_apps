//
//  FancyButton.swift
//  ninix_final
//
//  Created by Ali on 1/11/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import IBAnimatable

class FancyButton: AnimatableButton {
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.white.cgColor
        self.tintColor = UIColor.white
        self.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        self.titleLabel?.font = UIFont(name: "Playfair Display", size: 17.0)
        
        if self.state == .disabled {
            
            self.layer.backgroundColor = UIColor.clear.cgColor
            self.tintColor = UIColor.white
        }
        else {
            
            self.layer.backgroundColor = UIColor.white.cgColor
            self.tintColor = ColorPallete.secondary
        }
        
    }
    
    
    
}
