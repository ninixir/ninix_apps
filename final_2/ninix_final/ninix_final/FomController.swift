//
//  ScrollViewAdjustment.swift
//  ninix_final
//
//  Created by Ali on 1/11/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import IBAnimatable
import ReactiveKit
import Bond
import PMAlertController

class FormController: UIScrollableViewController, UITextFieldDelegate {
    
    
    var formTextFields: [UITextField] = []
    var labelFields: [AnimatableLabel] = []
    var submitButton: FancyButton = FancyButton()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    // MARK: Handle Status Bar
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        
        submitButton.addTarget(self, action: #selector(FormController.submit(_:)), for: .touchUpInside)
        submitButton.setTitleColor(ColorPallete.secondary, for: .normal)
        submitButton.setTitleColor(UIColor.lightText, for: .disabled)
        self.getTextFields(view: self.view)
        self.addLabelForTextFields()
        self.delegateAllFields()
        super.viewDidLoad()
        
    }
    
    func submit(_ sender: FancyButton) {
        
    }
    
    func addFromController(placeHolder: UIStackView) {
        placeHolder.addArrangedSubview(submitButton)
        placeHolder.addArrangedSubview(activityIndicator)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addObserverToTextFields()
    }
    
    func whoIsResponder(current: UITextField, textFields: [UITextField]) {
        current.resignFirstResponder()
        if textFields.contains(current) {
            if let index = textFields.index(of: current) {
                if index < textFields.count - 1 {
                    textFields[index + 1].becomeFirstResponder()
                }
            }
        }
    }
    
    private func getTextFields(view: UIView) {
        for subView in view.subviews {
            if subView is UITextField {
                formTextFields.append(subView as! UITextField)
            }
            else {
                if view.subviews.count > 0 {
                    self.getTextFields(view: subView)
                }
                
            }
        }
    }
    
    private func addLabelForTextFields() {
        
        for textField in formTextFields {
            let label = AnimatableLabel()
            
            label.text = textField.placeholder!
            label.font = UIFont(name: "Open Sans", size: 17)
            label.textColor = UIColor.white
            label.isHidden = true
            
            
            if let stack = textField.superview as? UIStackView {
                if let index = stack.arrangedSubviews.index(of: textField) {
                    stack.insertArrangedSubview(label, at: index)
                    self.labelFields.append(label)
                }
                
            }
            
        }
        
    }
    
    func addObserverToTextFields() {
        for (index,textField) in formTextFields.enumerated() {
            let _ = textField.reactive.text.observeNext { value in
                
                
                self.enableButton()
                if value != "" {
                    if self.labelFields[index].isHidden == true {
                        
                        DispatchQueue.main.async(){
                            self.labelFields[index].slide(.in, direction: .left)
                        }
                        self.labelFields[index].isHidden = false
                        
                    }
                    
                }
                else {
                    self.labelFields[index].isHidden = true
                }
                
            }
        }
    }
    
    func enableButton() {
        var enable = true
        for field in self.formTextFields {
            enable = enable && (field.text != "")
        }
        
        submitButton.isEnabled = enable
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.whoIsResponder(current: textField, textFields: self.formTextFields)
        return false
    }
    
    func delegateAllFields() {
        for textfield in formTextFields {
            textfield.delegate = self
        }
    }
    
    func disableTextFields() {
        for textField in formTextFields {
            textField.isEnabled = false
        }
    }
    
    func enableTextFields() {
        for textField in formTextFields {
            textField.isEnabled = true
        }
    }
    
    func startRequest() {
        self.disableTextFields()
        self.submitButton.isHidden = true
        self.activityIndicator.startAnimating()
    }
    
    func endRequest() {
        self.enableTextFields()
        submitButton.isHidden = false
        activityIndicator.stopAnimating()
    }
    
    func showError(title: String = "Error", decription: String, image: UIImage? = nil, style: PMAlertControllerStyle = .alert, actionTitle: String = "ok") {
        let alertVC = PMAlertController(title: title, description: decription, image: image, style: style)
        
        alertVC.addAction(PMAlertAction(title: actionTitle, style: .cancel, action: {
            self.endRequest()
        }))
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
}
























