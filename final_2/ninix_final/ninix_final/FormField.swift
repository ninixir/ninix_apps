//
//  ValidatorRuleClasses.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class FormField {
    var textField: UITextField
    var rules: [ValidationRule]
    var name: String? = nil
    var error: String?
    
    init(_ textField: UITextField) {
        self.textField = textField
        self.rules = []
        
    }
    
    func setName(name: String) -> FormField {
        self.name = name
        return self
    }
    
    func addRule(rule: ValidationRule) -> FormField {
        self.rules.append(rule)
        return self
    }
    
    func checkRules() -> Bool {
        for rule in rules {
            if rule.validate(value: textField.text!) == false {
                self.error = "\(name!) \(rule.error!)"
                return false
            }
        }
        return true
    }
    
    func end() {
        
    }
}

protocol ValidationRule {
    var error: String? {get set}
    func validate(value: String) -> Bool
}










