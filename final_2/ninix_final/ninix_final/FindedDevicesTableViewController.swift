//
//  FindedDevicesTableViewController.swift
//  ninix_final
//
//  Created by Ali on 12/31/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import CoreBluetooth
import SwiftOverlays

class FindedDevicesTableViewController: UITableViewController, NXCentralManagerDelegate {
    
    var discoveredDevices: [BLEDevice]!
    var bluetoothStateLabel: UILabel!
    
    @IBOutlet weak var viewHolder: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        discoveredDevices = []
        NXCentralManager.getInstance().scan = true
        NXCentralManager.getInstance().centralManagerDidUpdateState(NXCentralManager.getInstance().centralManager)
        NXCentralManager.getInstance().delegate = self
        self.createBluetoothStateLabel()
        
        if AppStateService.didAppIntroduced() {
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(FindedDevicesTableViewController.cancel(_:)))
        }
        
        if NXCentralManager.getInstance().centralManager.state == .poweredOff {
            bluetoothStateLabel.isHidden = false
            activityIndicator.stopAnimating()
        }
        
    }
    
    func cancel(_ sender: UIBarButtonItem) {
        Router.routeToMain()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoveredDevices.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bleDevices", for: indexPath)
        cell.imageView?.image = UIImage(named: "BLE")
        cell.textLabel?.text = discoveredDevices[indexPath.row].localName
        cell.detailTextLabel?.text = discoveredDevices[indexPath.row].isConnectable ? "connectable" : "can't connect"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        activityIndicator.stopAnimating()
        self.showWaitOverlayWithText("connecting...")
        NXCentralManager.getInstance().connect(selected: self.discoveredDevices[indexPath.row])
        
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func nxCentralManager(_ central: CBCentralManager, didDiscover device: BLEDevice) {
        self.discoveredDevices.append(device)
        tableView.reloadData()
    }
    
    func nxCentralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        performSegue(withIdentifier: "didConnect", sender: self)
    }
    
    func nxCentralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOff {
            bluetoothStateLabel.isHidden = false
            activityIndicator.stopAnimating()
        }
        else if central.state == .poweredOn {
            bluetoothStateLabel.isHidden = true
            activityIndicator.startAnimating()
        }
    }
    
    func createBluetoothStateLabel() {
        let size = CGFloat(30)
        let frame = CGRect(x: 0, y: self.view.frame.size.height / 2 - size, width: self.view.frame.size.width, height: size)
        bluetoothStateLabel = UILabel(frame: frame)
        bluetoothStateLabel.text = "Bluetooth is Off"
        bluetoothStateLabel.font = UIFont(name: "Open Sans", size: size)
        bluetoothStateLabel.isHidden = true
        bluetoothStateLabel.textColor = UIColor.red
        bluetoothStateLabel.textAlignment = .center
        self.view.addSubview(bluetoothStateLabel)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
