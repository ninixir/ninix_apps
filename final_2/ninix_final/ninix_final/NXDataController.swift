//
//  NXDataController.swift
//  ninix_final
//
//  Created by Ali on 2/6/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class NXDataController {
    
    var packet: Data!
    var derivedData: NXBluetoothData!
    var delegate: NXPeripheralDelegate?
    
    init(packet: Data, delegate: NXPeripheralDelegate?) {
        self.packet = packet
        self.delegate = delegate
        self.boot()
    }
    
    func deriveStats() {
        let code: Int16 = -8
        var temp16bit: Int16 = 0
        var temp8bit: Int8 = 0
        
        memcpy(&temp16bit, [packet[2], packet[1]], 2)
        let hightResTemperature = -46.85 + (Double)(temp16bit & code) * (175.72 / 65536)
        
        memcpy(&temp16bit, [packet[4], packet[3]], 2)
        let hightResHumidity = -6 + (Double)(temp16bit & code) * (125 / 65536)
        
        memcpy(&temp8bit, [packet[5]], 1)
        let highResRespiratory = (Double)(Int16(temp8bit) + 2) * 1.1
        
        memcpy(&temp8bit, [packet[6]], 1)
        let orientation = temp8bit
        
        
        let temperature = Float(hightResTemperature)
        let humidity = Float(hightResHumidity)
        let respiratory = Int16(round(highResRespiratory))
        
        self.derivedData = NXBluetoothData(temperature: temperature, humidity: humidity, respiratory: respiratory, orientation: orientation)
        let timerQueue = DispatchQueue(label: "com.ninixco.timer", attributes: .concurrent)
        timerQueue.async {
            NXLastReceivedData.data = self.derivedData
            NXLastReceivedData.time = Date()
        }
        
    }
    
    func save() {
        
        let saveQueue = DispatchQueue(label: "com.ninixco.save", attributes: .concurrent)
        saveQueue.async {
            NXVitalSignsController().save(object: self.derivedData.toNXVitalSignsModel())
            self.sync()
        }
        
    }
    
    func check() {
        let checkQueue = DispatchQueue(label: "com.ninixco.check", attributes: .concurrent)
        checkQueue.async {
            NXHealthChecker(data: self.derivedData).analyse()
        }
    }
    
    func boot() {
        self.deriveStats()
        self.delegate?.peripheral?(didUpdateValue: derivedData)
        self.save()
        //self.check()
    }
    
    func sync() {
        
        guard let lastUnsyncedTime = NXVitalSignsController.lastSyncedTime?.timeIntervalSinceNow else {
            return
        }
        
        print(-lastUnsyncedTime, "last seen data time", NXVitalSignsController.didNotSyncNumber(), " number of unsynced")
        if (NXVitalSignsController.didNotSyncNumber() > 5 && -lastUnsyncedTime > 30.0) ||  -lastUnsyncedTime > 180.0 {
            let syncQueue = DispatchQueue(label: "com.ninixo.internetSync", attributes: .concurrent)
            syncQueue.async {
                NXStreamDataHandler().sync()
            }
        }
    }
    
    @objc static func handleLastData(_ sender: Any) {
        
        if let data =  NXLastReceivedData.data, let time = NXLastReceivedData.time {
            if -time.timeIntervalSinceNow > NXHealthStandard.thresholdTimeToSave {
                print("save data from tick")
                NXVitalSignsController().save(object: data.toNXVitalSignsModel(), force: true)
            }
        }
        print("timer ticked <NXDataController:handleLastData>")
    }
    
}

