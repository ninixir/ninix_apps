//
//  BorderedView.swift
//  ninix_final
//
//  Created by Ali on 1/29/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class BorderedView: UIView {

    
    var borderTop: Bool = false
    var borderLeft: Bool = false
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if borderTop {
            let border = CALayer()
            border.borderWidth = 1
            border.borderColor = UIColor.darkGray.cgColor
            let frame = CGRect(x: self.frame.size.width / 6, y: -0.5, width: self.frame.size.width * 2 / 3, height: 1)
            border.frame = frame
            self.layer.masksToBounds = true
            self.layer.addSublayer(border)
        }
        
        if borderLeft {
            let border = CALayer()
            border.borderWidth = 1
            border.borderColor = UIColor.darkGray.cgColor
            let frame = CGRect(x: -0.5, y: self.frame.size.height / 6, width: 1, height: self.frame.size.height * 2 / 3)
            border.frame = frame
            self.layer.masksToBounds = true
            self.layer.addSublayer(border)
        }
        
    }
 

}
