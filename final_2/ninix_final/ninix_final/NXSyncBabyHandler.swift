//
//  NXSyncBabyHandler.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import Alamofire

class NXSyncBabyHandler: InternetService, AuthenticationHeader {
    
    let address = NXWebServiceAddress.syncBaby
    var babyInformation: BabyInformation!
    var delegate: NXSyncBabyHandlerDelegate?
    
    
    func with(babyInformation: BabyInformation)  {
        self.babyInformation = babyInformation
    }
    
    
    func sync() {
        sessionManager.request(self.address, method: .post, parameters: self.parameters(), encoding: JSONEncoding.default, headers: self.header).validate().responseObject(queue: self.queue) { (response: DataResponse<NXBabyJson>) in
            
            switch response.result {
                
            case .success:
                print(response.result.value ?? "nil", "response result value <NXSignupHandler:request>")
                self.validateResponse(response: response.result.value)
            case .failure(let error) :
                self.delegate?.didReceiveError(error: error.localizedDescription)
            }
            
        }
    }
    
    func retrieve() {
        sessionManager.request(self.address, method: .get, encoding: JSONEncoding.default, headers: self.header).validate().responseObject(queue: self.queue) { (response: DataResponse<NXBabyJson>) in
            
            switch response.result {
                
            case .success:
                print(response.result.value ?? "nil", "response result value <NXSignupHandler:request>")
                self.validateResponse(response: response.result.value)
            case .failure(let error) :
                self.delegate?.didReceiveError(error: error.localizedDescription)
            }
            
        }
    }
    
    func parameters() -> Parameters {
        var parameters: Parameters = [:]
        
        if let name = babyInformation?.name {
            parameters["name"] = name
        }
        if let weight = babyInformation?.weight {
            parameters["weight"] = weight
        }
        if let height = babyInformation?.height {
            parameters["height"] = height
        }
        if let head = babyInformation?.headCircumference {
            parameters["head"] = head
        }
        if let birthdate = babyInformation?.birthday {
            parameters["birth_date"] = birthdate.timeIntervalSince1970
        }
        if let gender = babyInformation?.gender {
            parameters["gender"] = gender.rawValue
        }
        
        
        return parameters
    }
    
    func validateResponse(response: NXBabyJson?) {
        guard let response = response, let code = response.code else {
            delegate?.didReceiveServerError()
            return
        }
        
        if code == 0, let babyInformation = response.result {
            self.save(babyJsonModel: babyInformation)
            delegate?.didUpdate()
            return
        }
        else {
            if let message = response.message_en {
                delegate?.didReceiveError(error: message)
                return
            }
        }
        delegate?.didReceiveServerError()
    }
    
    func save(babyJsonModel: NXBabyJsonModel) {
        var birthday: Date? = nil
        var gender: Gender? = nil
        if let birthdate = babyJsonModel.birth_date as? TimeInterval {
            birthday = Date(timeIntervalSince1970: birthdate)
        }
        if let genderRawValue = babyJsonModel.gender as? Int {
            gender = Gender(rawValue: genderRawValue)
        }
        
        let babyInformation = BabyInformation(name: babyJsonModel.name, birthday: birthday, weight: babyJsonModel.weight as Int?, height: babyJsonModel.height as Int?, headCircumference: babyJsonModel.head as Int?, gender: gender)
        
        NXUserDefault().saveBabyInformation(babyInformation)
    }
    
 
    
}



protocol NXSyncBabyHandlerDelegate: InternetServiceDelegate {
    
    func didUpdate()
    
}




