//
//  LegendLabel.swift
//  ninix_final
//
//  Created by Ali on 1/14/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class LegendLabel: UILabel {
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let fontSize = self.font.pointSize
        let color = self.textColor
        
        let leftLineFrame = CGRect(x: 0, y: self.frame.size.height / 2 - 0.5, width: self.frame.size.width / 2 - fontSize, height: 1)
        let rightLineFrame = CGRect(x: self.frame.size.width / 2 + fontSize, y: self.frame.size.height / 2 - 0.5, width: self.frame.size.width, height: 1)
        
        let leftLine = CALayer()
        leftLine.frame = leftLineFrame
        leftLine.backgroundColor = color?.cgColor
        
        let rightLine = CALayer()
        rightLine.frame = rightLineFrame
        rightLine.backgroundColor = color?.cgColor
        self.layer.masksToBounds = true
        
        self.textAlignment = .center
        self.layer.addSublayer(leftLine)
        self.layer.addSublayer(rightLine)
        
    }
    
    
}
