//
//  SettingTableViewController.swift
//  ninix_final
//
//  Created by Ali on 1/3/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController {
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Settings"
        
        
    }
    
    @IBAction func cancel(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

}
