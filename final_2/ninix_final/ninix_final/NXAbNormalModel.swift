//
//  NXAbNormalModel.swift
//  ninix_final
//
//  Created by Ali on 2/14/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXAbNormalModel: Object {
    dynamic var time: NSDate = NSDate()
    dynamic var invalid: Bool = false
    dynamic var valid: Bool = false
    dynamic var didNotify: Bool = false
}

class NXAbNormalTemperature: NXAbNormalModel {
    dynamic var value: Float = 0.0
}

class NXAbNormalRespiratory: NXAbNormalModel {
    dynamic var value: Int = 0
}

class NXAbNormalOrientation: NXAbNormalModel {
    dynamic var vlaue: Int = 1
}

class NXAbNormalHumidity: NXAbNormalModel {
    dynamic var value: Float = 0.0
}
