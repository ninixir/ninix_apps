
import UIKit
import ReactiveKit
import Bond

class NXValidator {
    var formFields: [FormField]
    var error: String?
    
    init() {
        self.formFields = []
    }
    
    func addFiled(textField: UITextField) -> FormField {
        let formField = FormField(textField)
        formFields.append(formField)
        return formField
    }
    
    func validate() -> Bool {
        
        
        for formField in self.formFields {
            if formField.checkRules() == false {
                if formField.checkRules() == false {
                    self.error = formField.error
                    return false
                }
            }
        }
        return true
        
    }
    
}





