//
//  SecondViewController.swift
//  ninix
//
//  Created by Ali on 10/6/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import CoreBluetooth

class DeviceViewController: ThemedUIViewController, NXCentralManagerDelegate {
    
    
    @IBOutlet weak var topConnectionLabel: UILabel!
    @IBOutlet weak var bottomConnectionLabel: UILabel!
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    @IBOutlet weak var disconnectButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        NXCentralManager.getInstance().delegate = self
        
        if #available(iOS 10.0, *) {
            showMessage(state: NXCentralManager.getInstance().centralManager.state)
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    @IBAction func addNewDevice(_ sender: AnyObject) {
        Router.routeToConnect()
    }
    
    @IBAction func disconnectFromDevice(_ sender: AnyObject) {
        
    }
    
    func nxCentralManagerDidUpdateState(_ central: CBCentralManager) {
        if #available(iOS 10.0, *) {
            showMessage(state: central.state)
        } else {
            // Fallback on earlier versions
        }
        /*
        if central.state == .poweredOff {
            topConnectionLabel.text = "Bluetooth is Off!"
            bottomConnectionLabel.text = "Turn on Bluetooth, and tap + to connect new device"
        }
        else if central.state == .poweredOn {
            if NXPeripheral.getInstance().peripheral?.state == .connected {
                topConnectionLabel.text = "Device Connected!"
                bottomConnectionLabel.text = "Recieving data..."
            }
            else {
                topConnectionLabel.text = "No device connected yet"
                bottomConnectionLabel.text = "Tap + to connect new device"
            }
        }
         */
    }
    
    func nxCentralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        topConnectionLabel.text = "Device Connected!"
        bottomConnectionLabel.text = "Recieving data..."
        addBarButton.isEnabled = false
        disconnectButton.isHidden = false
    }
    
    @available(iOS 10.0, *)
    func showMessage(state: CBManagerState) {
        if state == .poweredOff {
            topConnectionLabel.text = "Bluetooth is Off!"
            bottomConnectionLabel.text = "Turn on Bluetooth, and tap + to connect new device"
            addBarButton.isEnabled = true
            disconnectButton.isHidden = true
        }
        else if state == .poweredOn {
            if NXPeripheral.getInstance().peripheral?.state == .connected {
                topConnectionLabel.text = "Device Connected!"
                bottomConnectionLabel.text = "Recieving data..."
                addBarButton.isEnabled = false
                disconnectButton.isHidden = false
            }
            else {
                topConnectionLabel.text = "No device connected yet"
                bottomConnectionLabel.text = "Tap + to connect new device"
                addBarButton.isEnabled = true
                disconnectButton.isHidden = true
            }
        }
    }
    
    
    
    
    
    
    
    
    
}

