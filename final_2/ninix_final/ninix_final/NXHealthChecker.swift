//
//  NXHealthChecker.swift
//  ninix_final
//
//  Created by Ali on 2/4/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation


class NXHealthChecker {
    
    let data: NXBluetoothData!
    init(data: NXBluetoothData) {
        self.data = data
    }
    
    func checkTemperature(_ temperature: Float? = nil) -> NXHealthStatus {
        let temperature: Float = temperature ?? self.data.temperature
        if temperature >= NXHealthStandard.temperature.high || temperature <= NXHealthStandard.temperature.low {
            return NXHealthStatus.danger
        }
        if temperature >= NXHealthStandard.temperature.up || temperature <= NXHealthStandard.temperature.down {
            return NXHealthStatus.warning
        }
        return NXHealthStatus.normal
    }
    
    func checkHumidity(_ humidity: Float? = nil) -> NXHealthStatus {
        let humidity: Float = humidity ?? self.data.humidity
        if humidity >= NXHealthStandard.humidity.high  {
            return NXHealthStatus.pooped
        }
        if humidity >= NXHealthStandard.humidity.up  {
            return NXHealthStatus.pooped
        }
        return NXHealthStatus.normal
    }
    
    func checkRespiratory(_ respiratory: Int16? = nil) -> NXHealthStatus {
        let respiratory: Int16 = respiratory ?? self.data.respiratory
        if respiratory >= NXHealthStandard.respiratory.high || respiratory <= NXHealthStandard.respiratory.low {
            return NXHealthStatus.danger
        }
        if respiratory >= NXHealthStandard.respiratory.up || respiratory <= NXHealthStandard.respiratory.down {
            return NXHealthStatus.warning
        }
        return NXHealthStatus.normal
    }
    
    
    
    func checkOrientation(_ orientation: Int8? = nil) -> NXHealthStatus {
        let orientation: Int8 = orientation ?? self.data.orientation
        if orientation == 1 {
            return NXHealthStatus.onStomach
        }
        return NXHealthStatus.normal
    }
    
    
    
    func analyse() {
        
        
        
        print("start analyse <NXHealthChecker:analyse>")
        analyseTemperature()
        analyseRespiratory()
        analyseHumidity()
        analyseOrientation()
        
        
    }
    
    func analyseTemperature() {
        print("start analyse temperature")
        if checkTemperature() != . normal {
            print("temperature is abnormal")
            let notificationQueue = DispatchQueue(label: "com.ninixco.notification.temperature", attributes: .concurrent)
            notificationQueue.async {
                guard let value = NXAbNormalTemperatureController(time: NXHealthStandard.temperatureTime).getAnalyzedValue(object: NXAbNormalTemperature(value: ["value": self.data.temperature])) else {
                    return
                }
                
                if self.checkTemperature(Float(value)) != .normal {
                    let type = NXNotificationType.temperature
                    let status = self.checkTemperature(Float(value))
                    
                    self.saveNotification(type: type, status: status, value: value)
                }
            }
            
        }
    }
    
    func analyseRespiratory() {
        if checkRespiratory() != .normal {
            let notificationQueue = DispatchQueue(label: "com.ninixco.notification.respiratory", attributes: .concurrent)
            notificationQueue.async {
                guard let value = NXAbNormalRespiratoryController(time: NXHealthStandard.respiratoryTime).getAnalyzedValue(object: NXAbNormalRespiratory(value: ["value": self.data.respiratory])) else {
                    return
                }
                
                if self.checkRespiratory(Int16(value)) != .normal {
                    let type = NXNotificationType.respiratory
                    let status = self.checkRespiratory(Int16(value))
                    
                    self.saveNotification(type: type, status: status, value: value)
                }
            }
            
        }
    }
    
    func analyseHumidity() {
        if checkHumidity() != .normal {
            let notificationQueue = DispatchQueue(label: "com.ninixco.notification.humidity", attributes: .concurrent)
            notificationQueue.async {
                guard let value = NXAbNormalHumidityController(time: NXHealthStandard.humidityTime).getAnalyzedValue(object: NXAbNormalHumidity(value: ["value": self.data.humidity])) else {
                    return
                }
                
                if self.checkHumidity(Float(value)) != .normal {
                    let type = NXNotificationType.humidity
                    let status = self.checkHumidity(Float(value))
                    
                    self.saveNotification(type: type, status: status, value: value)
                }
            }
            
        }
    }
    
    func analyseOrientation() {
        if checkOrientation() != .normal {
            let notificationQueue = DispatchQueue(label: "com.ninixco.notification.orientation", attributes: .concurrent)
            notificationQueue.async {
                guard let value = NXAbNormalOrientationController(time: NXHealthStandard.humidityTime).getAnalyzedValue(object: NXAbNormalOrientation(value: ["value": self.data.humidity])) else {
                    return
                }
                
                let orientaionValue: Int8 = value > 0.7 ? 1 : 0
                
                if self.checkOrientation(orientaionValue) != .normal {
                    let type = NXNotificationType.orientation
                    let status = self.checkOrientation(orientaionValue)
                    
                    self.saveNotification(type: type, status: status, value: value)
                }
            }
            
        }
    }
    
    func saveNotification(type: NXNotificationType, status: NXHealthStatus, value: Double) {
        NXNotificationController().save(object: NXNotificationModel(value: [
            "type": type.rawValue,
            "status": status.rawValue,
            "value": value
            ]))
    }
    
}

enum NXHealthStatus: Int {
    case normal = 0, warning, danger, mayBePooped ,pooped, onStomach
}





