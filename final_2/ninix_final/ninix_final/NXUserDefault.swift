//
//  NXUserDefault.swift
//  ninix_final
//
//  Created by Ali on 1/30/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class NXUserDefault {
    
    let defaults: UserDefaults!
    
    init() {
        defaults = UserDefaults.standard
    }
    
    
    func saveUserInformation(fullName: String? = nil, username: String? = nil, birthday: Date? = nil, children: Int? = nil, email: String? = nil, relation: Relation? = nil, phone: String? = nil, address: String? = nil, registerAt: Date? = nil) {
        let userInformation = UserInformation(fullName: fullName, username: username, birthday: birthday, children: children, email: email, relation: relation, phone: phone, address: address, registerAt: registerAt)
        save(info: userInformation, key: DefaultKeys.userInformation)
    }
    
    func saveUserInformation(_ information: UserInformation) {
        save(info: information, key: DefaultKeys.userInformation)
    }
    
    func saveBabyInformation(name: String? = nil, birthday: Date? = nil, weight: Int? = nil, height: Int? = nil, headCircumference: Int? = nil, gender: Gender? = nil) {
        let babayInformation = BabyInformation(name: name, birthday: birthday, weight: weight, height: height, headCircumference: headCircumference, gender: gender)
        save(info: babayInformation, key: DefaultKeys.babyInformation)
    }
    
    func saveBabyInformation(_ information: BabyInformation) {
        save(info: information, key: DefaultKeys.babyInformation)
    }
    
    
    private func save(info: Any, key: DefaultKeys) {
        let queue = DispatchQueue.global(qos: .default)
        queue.async {
            let encrypted = NSKeyedArchiver.archivedData(withRootObject: info)
            self.defaults.set(encrypted, forKey: key.rawValue)
            self.defaults.synchronize()
        }
    }
    
    private func retrive(key: DefaultKeys) -> Data? {
        return defaults.object(forKey: key.rawValue) as? Data
    }
    
    func retrieveUserInformation() -> UserInformation {
        
        if let encryptedUserInformation = retrive(key: DefaultKeys.userInformation) {
            if let userInformation = NSKeyedUnarchiver.unarchiveObject(with: encryptedUserInformation) as? UserInformation {
                return userInformation
            }
        }
        
        return UserInformation()
    }
    
    func retrieveBabyInformation() -> BabyInformation {
        
        if let encryptedUserInformation = retrive(key: DefaultKeys.babyInformation) {
            if let babyInformation = NSKeyedUnarchiver.unarchiveObject(with: encryptedUserInformation) as? BabyInformation {
                return babyInformation
            }
        }
        
        return BabyInformation()
    }
}
