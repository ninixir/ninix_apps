//
//  UIScrollableViewController.swift
//  ninix_final
//
//  Created by Ali on 1/25/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import IBAnimatable

class UIScrollableViewController: UIViewController {
    
    var initViewHeightSize: CGFloat!
    
    override func viewDidLoad() {
        
        
        self.initViewHeightSize = self.view.frame.size.height
        
        let queue = DispatchQueue.global(qos: .default)
        queue.async {
            NotificationCenter.default.addObserver(self, selector:
                #selector(FormController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            
            NotificationCenter.default.addObserver(self, selector:
                #selector(FormController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        }
        UIApplication.shared.statusBarStyle = .lightContent
        super.viewDidLoad()
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            
            if view.frame.size.height == self.initViewHeightSize {
                
                var height: CGFloat = CGFloat(0)
                for subview in view.subviews {
                    if subview.alpha != 0 {
                        height += subview.frame.size.height
                    }
                    
                }
                
                view.frame.size.height = height + 150.0
                
                if let scrollView = view as? UIScrollView {
                    scrollView.bounces = false
                    
                    scrollView.contentSize = CGSize(width: view.frame.size.width, height: view.frame.size.height)
                    scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: view.frame.size.height + keyboardSize.height - self.initViewHeightSize, right: 0.0)
                }
                else {
                    print("not found scrollview")
                }
                
            }
            
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        self.view.frame.size.height = self.initViewHeightSize
        if let scrollView = self.view as? UIScrollView {
            scrollView.contentSize = CGSize(width: 0.0, height: 0.0)
            scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        }
        
    }
    
}
