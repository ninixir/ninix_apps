//
//  FourthPageViewController.swift
//  ninix_final
//
//  Created by Ali on 12/28/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import IBAnimatable

class FourthPageViewController: UIViewController {

    @IBOutlet weak var image: AnimatableImageView!
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startButton.backgroundColor = ColorPallete.primary
        startButton.tintColor = ColorPallete.white
        startButton.layer.cornerRadius = 8
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        image.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        image.alpha = 1
        image.slideFade(.in, direction: .right)
        
    }
    
    @IBAction func start(_ sender: AnyObject) {
        AppStateService.appIntroduced()
        Router.routeToConnect()
    }

    
}
