//
//  NXSyncUserHandler.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import Alamofire

class NXSyncUserHandler: InternetService, AuthenticationHeader {
    
    let address = NXWebServiceAddress.syncUser
    var userInformation: UserInformation!
    var delegate: NXSyncUserHandlerDelegate?
    
    func with(userInformation: UserInformation) {
        self.userInformation = userInformation
    }
    
    func sync() {
        
        sessionManager.request(self.address, method: .post, parameters: self.parameters(), encoding: JSONEncoding.default, headers: self.header).validate().responseObject(queue: self.queue) { (response: DataResponse<NXUserJson>) in
            
            switch response.result {
                
            case .success:
                print(response.result.value ?? "nil", "response result value <NXSignupHandler:request>")
                self.validateResponse(response: response.result.value)
            case .failure(let error) :
                self.delegate?.didReceiveServerError()
                print(error, " error <NXSyncUserHandler:request>")
                
            }
            
        }
    }
    
    func retrieve() {
        sessionManager.request(self.address, method: .get, encoding: JSONEncoding.default, headers: self.header).validate().responseObject(queue: self.queue) { (response: DataResponse<NXUserJson>) in
            
            switch response.result {
                
            case .success:
                print(response.result.value ?? "nil", "response result value <NXSignupHandler:request>")
                self.validateResponse(response: response.result.value)
            case .failure(let error) :
                self.delegate?.didReceiveServerError()
                print(error, " error <NXSyncUserHandler:request>")
            }
            
        }
    }
    
    func parameters() -> Parameters {
        var parameters: Parameters = [:]
        
        if let fullname = userInformation?.fullName {
            parameters["fullname"] = fullname
        }
        if let username = userInformation?.username {
            parameters["username"] = username
        }
        if let birthdate = userInformation?.birthday {
            parameters["birth_date"] = birthdate.timeIntervalSince1970
        }
        if let children = userInformation?.children {
            parameters["children"] = children
        }
        if let email = userInformation?.email {
            parameters["email"] = email
        }
        if let relation = userInformation?.relation {
            parameters["relation"] = relation.rawValue
        }
        if let phone = userInformation?.phone {
            parameters["phone"] = phone
        }
        if let address = userInformation?.address {
            parameters["address"] = address
        }
        
        return parameters
    }
    
    func validateResponse(response: NXUserJson?) {
        guard let response = response, let code = response.code else {
            delegate?.didReceiveServerError()
            return
        }
        
        if code == 0, let userInformation = response.result {
            self.save(userJsonModel: userInformation)
            delegate?.didUpdate()
            return
        }
        else {
            if let message = response.message_en {
                delegate?.didReceiveError(error: message)
                return
            }
        }
        delegate?.didReceiveServerError()
    }
    
    func save(userJsonModel: NXUserJsonModel) {
        
        var birthday: Date? = nil
        var relation: Relation? = nil
        if let birthdate = userJsonModel.birth_date as? TimeInterval {
            birthday = Date(timeIntervalSince1970: birthdate)
        }
        if let relationRawValue = userJsonModel.relation as? Int {
            relation = Relation(rawValue: relationRawValue)
        }
        let userInformation = UserInformation(fullName: userJsonModel.fullname, username: userJsonModel.username, birthday: birthday, children: userJsonModel.children as? Int, email: userJsonModel.email, relation: relation, phone: userJsonModel.phone, address: userJsonModel.address)
        
        NXUserDefault().saveUserInformation(userInformation)
         
    }
    
}



protocol NXSyncUserHandlerDelegate: InternetServiceDelegate {
    
    func didUpdate()
    
}




