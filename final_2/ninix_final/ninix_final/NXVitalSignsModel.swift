//
//  NXVitalSignsModel.swift
//  ninix_final
//
//  Created by Ali on 1/31/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXVitalSignsModel: NXShouldSyncModel {
    
    dynamic var temperature: Float = 0
    dynamic var humidity: Float = 0
    dynamic var respiratory = 0
    dynamic var orientation = 0
    
    
    static func == (left: NXVitalSignsModel, right: NXVitalSignsModel) -> Bool {
        if Swift.abs(left.temperature - right.temperature) < 0.1,
        Swift.abs(left.humidity - right.humidity) <= 1,
        left.respiratory == right.respiratory,
        left.orientation == right.orientation,
            Swift.abs(left.time.timeIntervalSince1970 - right.time.timeIntervalSince1970) < NXHealthStandard.thresholdTimeToSave {
            return true
        }
        return false
    }
    
    static func === (left: NXVitalSignsModel, right: NXVitalSignsModel) -> Bool {
        if Swift.abs(left.temperature - right.temperature) < 0.1,
            Swift.abs(left.humidity - right.humidity) <= 1,
            left.respiratory == right.respiratory,
            left.orientation == right.orientation,
            left.time.timeIntervalSince1970 == right.time.timeIntervalSince1970 {
            return true
        }
        return false
    }
    
    static func != (left: NXVitalSignsModel, right: NXVitalSignsModel) -> Bool {
        return !(left == right)
    }
    
    func toBluetoothData() -> NXBluetoothData {
        return NXBluetoothData(temperature: self.temperature, humidity: self.humidity, respiratory: Int16(self.respiratory), orientation: Int8(self.orientation))
    }
    
}
