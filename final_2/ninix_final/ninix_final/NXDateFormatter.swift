//
//  NXDateFormatter.swift
//  ninix_final
//
//  Created by Ali on 1/30/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class NXDateFormatter {
    
    var date: Date
    
    init(date: Date) {
        self.date = date
    }
    
    func toSecond() -> Int {
        return -(Int)(self.date.timeIntervalSinceNow)
    }
    
    func toMinute() -> Int {
        return self.toSecond() / 60
    }
    
    func toHour() -> Int {
        return self.toMinute() / 60
    }
    
    func toDay() -> Int {
        return self.toHour() / 24
    }
    
    func toWeek() -> Int {
        return self.toDay() / 7
    }
    
    func toMonth() -> Int {
        return self.toDay() / 30
    }
    
    func toYear() -> Int {
        return self.toWeek() / 52
    }
    
    func toAgo() -> String {
        if self.toWeek() > 0 {
            return "\(self.toWeek())w"
        }
        else if self.toDay() > 0 {
            return "\(self.toDay())d"
        }
        else if self.toHour() > 0 {
            return "\(self.toHour())h"
        }
        else if self.toMinute() > 0 {
            return "\(self.toMinute())m"
        }
        else {
            return "\(self.toSecond())s"
        }
    }
}
