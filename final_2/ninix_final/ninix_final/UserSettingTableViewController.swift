//
//  UserSettingTableViewController.swift
//  ninix_final
//
//  Created by Ali on 1/3/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond

class UserSettingTableViewController: UITableViewController, NXSyncUserHandlerDelegate, ShowPopupMessage {
    
    @IBOutlet weak var fullnameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextView!
    @IBOutlet weak var childrenStepper: UIStepper!
    @IBOutlet weak var childrenLabel: UILabel!
    @IBOutlet weak var relationSegment: UISegmentedControl!
    @IBOutlet weak var birthdayDatePicker: UIDatePicker!
    
    
    var userInformation: UserInformation!
    var syncUserHandler: NXSyncUserHandler!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "User Settings"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "save", style: .done, target: self, action: #selector(UserSettingTableViewController.save(button:)))
        
        userInformation = NXUserDefault().retrieveUserInformation()
        
        syncUserHandler = NXSyncUserHandler()
        syncUserHandler.with(userInformation: userInformation)
        syncUserHandler.delegate = self
        self.binding()
        
        
        
        
        
        
    }
    
    func didUpdate() {
        print("success")
        DispatchQueue.main.async {
            self.navigationController?.removeFromParentViewController()
        }
        
        
    }
    
    func didReceiveServerError() {
        print("something went wrong in server <UserSettingTableViewController:didReceiveServerError>")
        self.showError(decription: "Something went wrong in server, try again after few second", viewController: self)
    }
    
    func didReceiveError(error: String) {
        print("\(error) <UserSettingTableViewController:didReceiveError>")
        self.showError(decription: error, viewController: self)
    }
    
    func save(button: UIBarButtonItem) {
        
        userInformation.fullName = fullnameTextField.text != "" ? fullnameTextField.text : nil
        userInformation.username = usernameTextField.text != "" ? usernameTextField.text : nil
        userInformation.email = emailTextField.text != "" ? emailTextField.text : nil
        userInformation.phone = phoneTextField.text != "" ? phoneTextField.text : nil
        userInformation.address = addressTextField.text != "" ? addressTextField.text : nil
        userInformation.children = childrenLabel.text != "0" ? Int(childrenLabel.text!) : nil
        
        self.syncUserHandler.sync()
        //NXUserDefault().saveUserInformation(userInformation)
        //navigationController?.removeFromParentViewController()
        
    }
    
    func binding() {
        fullnameTextField.text = userInformation.fullName != nil ? userInformation.fullName! : ""
        usernameTextField.text = userInformation.username != nil ? userInformation.username! : ""
        emailTextField.text = userInformation.email != nil ? userInformation.email! : ""
        phoneTextField.text = userInformation.phone != nil ? userInformation.phone! : ""
        addressTextField.text = userInformation.address != nil ? userInformation.address : ""
        childrenLabel.text = userInformation.children != nil ? "\(userInformation.children!)" : "0"
        childrenStepper.value = Double(childrenLabel.text!)!
        
        if let relation = userInformation.relation {
            if relation == .father {
                relationSegment.selectedSegmentIndex = 0
            }
            else if relation == .mother {
                relationSegment.selectedSegmentIndex = 1
            }
            else if relation == .other {
                relationSegment.selectedSegmentIndex = 2
            }
        }
        
        if let date = userInformation.birthday {
            birthdayDatePicker.date = date
        }
    }
    
    
    @IBAction func childrenStepperChanged(_ sender: UIStepper) {
        childrenLabel.text = Int(sender.value).description
        userInformation.children = Int(sender.value)
    }
    
    @IBAction func changeRelationSegment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            userInformation.relation = .father
        }
        else if sender.selectedSegmentIndex == 1 {
            userInformation.relation = .mother
        }
        else {
            userInformation.relation = .other
        }
    }
    
    @IBAction func birthdayChanged(_ sender: UIDatePicker) {
        userInformation.birthday = sender.date
    }
    
    
    

}
