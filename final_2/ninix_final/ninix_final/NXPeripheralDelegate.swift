//
//  NXPeripheralDelegate.swift
//  ninix_final
//
//  Created by Ali on 1/21/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc protocol NXPeripheralDelegate {
    
    @objc optional func peripheral(temperature updateValue: Float)
    @objc optional func peripheral(humidity updateValue: Float)
    @objc optional func peripheral(respiratory updateValue: Int16)
    @objc optional func peripheral(orientation updateValue: Int8)
    
    @objc optional func peripheral(didUpdateValue value: NXBluetoothData)
}
