//
//  UserInformation.swift
//  ninix_final
//
//  Created by Ali on 1/31/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import EVReflection
import ReactiveKit
import Bond

class UserInformation: NSObject, NSCoding {
    var fullName: String?
    var username: String?
    var birthday: Date?
    var children: Int?
    var email: String?
    var relation: Relation?
    var phone: String?
    var address: String?
    var registerAt: Date?
    
    
    init(fullName: String? = nil, username: String? = nil, birthday: Date? = nil, children: Int? = nil, email: String? = nil, relation: Relation? = nil, phone: String? = nil, address: String? = nil, registerAt: Date? = nil) {
        self.fullName = fullName
        self.username = username
        self.birthday = birthday
        self.children = children
        self.email = email
        self.relation = relation
        self.phone = phone
        self.address = address
        self.registerAt = registerAt
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let fullName = aDecoder.decodeObject(forKey: "fullName") as? String
        let username = aDecoder.decodeObject(forKey: "username") as? String
        let birthday = aDecoder.decodeObject(forKey: "birthday") as? Date
        let children = aDecoder.decodeObject(forKey: "children") as? Int
        let email = aDecoder.decodeObject(forKey: "email") as? String
        let phone = aDecoder.decodeObject(forKey: "phone") as? String
        let address = aDecoder.decodeObject(forKey: "address") as? String
        let registerAt =  aDecoder.decodeObject(forKey: "registerAt") as? Date
        
        var relation: Relation? = nil
        if let relationInt = aDecoder.decodeObject(forKey: "relation") as? Int {
            relation = Relation(rawValue: relationInt)
        }
        
        self.init(fullName: fullName, username: username, birthday: birthday, children: children, email: email, relation: relation, phone: phone, address: address, registerAt: registerAt)
    }
    
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(birthday, forKey: "birthday")
        aCoder.encode(children, forKey: "children")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(relation?.rawValue, forKey: "relation")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(registerAt, forKey: "registerAt")
    }
    
}
