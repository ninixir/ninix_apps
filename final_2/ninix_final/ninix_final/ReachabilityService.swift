//
//  ReachabilityService.swift
//  ninix_final
//
//  Created by Ali on 1/12/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import ReachabilitySwift

class ReachabilityService {
    
    let reachability = Reachability()!
    private static let instance = ReachabilityService()
    var state: ReachabilitytState?
    
    private init() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(note:)),name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        //self.internetAccess()
    }
    
    static func getInstance() -> ReachabilityService {
        return self.instance
    }
    
    @objc func reachabilityChanged(note: NSNotification) {
        print("reachability changed :}")
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                self.state = .wifi
            } else {
                self.state = .cellular
            }
        } else {
            self.state = .none
        }
    }
    
    
}


enum ReachabilitytState {
    case wifi, cellular, none
}
