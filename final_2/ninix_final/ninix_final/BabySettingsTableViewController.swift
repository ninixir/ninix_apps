//
//  BabySettingsTableViewController.swift
//  ninix_final
//
//  Created by Ali on 1/3/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit

class BabySettingsTableViewController: UITableViewController, UITextFieldDelegate, NXSyncBabyHandlerDelegate, ShowPopupMessage {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var headTextField: UITextField!
    @IBOutlet weak var genderSegment: UISegmentedControl!
    @IBOutlet weak var birthdayPicker: UIDatePicker!
    
    var babyInformation: BabyInformation!
    var syncBabyHandler: NXSyncBabyHandler!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "User Settings"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "save", style: .done, target: self, action: #selector(UserSettingTableViewController.save(button:)))
        
        babyInformation = NXUserDefault().retrieveBabyInformation()
        syncBabyHandler = NXSyncBabyHandler()
        syncBabyHandler.delegate = self
        syncBabyHandler.with(babyInformation: babyInformation)
        self.binding()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func didUpdate() {
        DispatchQueue.main.sync {
            self.navigationController?.removeFromParentViewController()
        }
        
    }
    
    func didReceiveServerError() {
        showError(decription: "something went wrong in server, after few second please try again", viewController: self)
    }
    
    func didReceiveError(error: String) {
        showError(decription: error, viewController: self)
    }
    
    func save(button: UIBarButtonItem) {
        
        let name = nameTextField.text == "" ? nil : nameTextField.text
        let weight = weightTextField.text == "" ? nil : Int(weightTextField.text!)
        let height = heightTextField.text == "" ? nil : Int(heightTextField.text!)
        let head = headTextField.text == "" ? nil : Int(headTextField.text!)
        
        babyInformation.name = name
        babyInformation.weight = weight
        babyInformation.height = height
        babyInformation.headCircumference = head
        
        
        //NXUserDefault().saveBabyInformation(babyInformation)
        syncBabyHandler.sync()
        
        //navigationController?.popViewController(animated: true)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    
    @IBAction func genderChanged(_ sender: AnyObject) {
        if genderSegment.selectedSegmentIndex == 0 {
            babyInformation.gender = Gender.boy
        }
        else {
            babyInformation.gender = Gender.girl
        }
        
    }
    
    @IBAction func birthdayChanged(_ sender: AnyObject) {
        babyInformation.birthday = birthdayPicker.date
    }
    
    func binding() {
        nameTextField.text = babyInformation.name != nil ? babyInformation.name! : ""
        weightTextField.text = babyInformation.weight != nil ? "\(babyInformation.weight!)" : ""
        heightTextField.text = babyInformation.height != nil ? "\(babyInformation.height!)" : ""
        headTextField.text = babyInformation.headCircumference != nil ? "\(babyInformation.headCircumference!)"  : ""
        
        if babyInformation.gender != nil {
            if babyInformation.gender == .boy {
                genderSegment.selectedSegmentIndex = 0
            }
            else {
                genderSegment.selectedSegmentIndex = 1
            }
        }
        
        if babyInformation.birthday != nil {
            birthdayPicker.date = babyInformation.birthday!
        }
    }
    
    

}
