//
//  NXNotificationLocal.swift
//  ninix_final
//
//  Created by Ali on 2/4/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import UserNotifications

@available(iOS 10.0, *)
class NXNotificationLocal {
    
    let content: UNMutableNotificationContent!
    
    
    init(title: String, subtitle: String? = nil, badge: NSNumber? = nil) {
        self.content = UNMutableNotificationContent()
        content.title = title
        content.sound = UNNotificationSound.default()
        if let subtitle = subtitle {
            content.body = subtitle
        }
        if let badge = badge {
            content.badge = badge
        }
        
    }
    
    func send(timeInterval: TimeInterval = 5) {
        DispatchQueue.main.async {
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
            let request = UNNotificationRequest(identifier: "notification", content: self.content, trigger: trigger) // Schedule the notification.
            let center = UNUserNotificationCenter.current()
            center.add(request) { (error : Error?) in
                if let theError = error {
                    print(theError, " <NXNotificationLocal:send>")
                }
            }
        }
        
    }
}
