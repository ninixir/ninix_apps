//
//  AlertView.swift
//  ninix_final
//
//  Created by Ali on 10/16/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit

class AlertHandlers {
    
    class func showSimpleAlertWithOk(title: String, message: String) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(alertAction)
        
        return alertController
    }
    
}
