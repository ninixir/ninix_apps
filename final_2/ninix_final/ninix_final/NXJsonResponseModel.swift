//
//  NXJsonResponseModel.swift
//  ninix_final
//
//  Created by Ali on 2/1/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import EVReflection

class NXResponseJson: EVNetworkingObject {
    var code: NSNumber?
    var message_en: String?
    var message_fa: String?
}

class NXTokenJson: EVNetworkingObject {
    var token_type: String?
    var access_token: String?
}

class NXRegisterJson: NXResponseJson {
    var result: NXTokenJson?
}

class NXLoginJson: NXResponseJson {
    var result: NXTokenJson?
}

class NXUserJson: NXResponseJson {
    var result: NXUserJsonModel?
}

class NXBabyJson: NXResponseJson {
    var result: NXBabyJsonModel?
}

class NXUserJsonModel: EVNetworkingObject {
    var fullname: String?
    var username: String?
    var birth_date: NSNumber?
    var children: NSNumber?
    var email: String?
    var relation: NSNumber?
    var phone: String?
    var address: String?
    
}

class NXBabyJsonModel: EVNetworkingObject {
    var name: String?
    var weight: NSNumber?
    var height: NSNumber?
    var head: NSNumber?
    var birth_date: NSNumber?
    var gender: NSNumber?
    
}

