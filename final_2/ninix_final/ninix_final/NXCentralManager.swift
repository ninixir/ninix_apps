//
//  NXCentralManager.swift
//  ninix_final
//
//  Created by Ali on 1/21/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth

class NXCentralManager: NSObject, CBCentralManagerDelegate {
    
    var centralManager: CBCentralManager!
    var delegate: NXCentralManagerDelegate?
    var scan: Bool = false
    
    private static let instance: NXCentralManager = NXCentralManager()
    
    private override init() {
        super.init()
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("central updated state <NXCentralManager:centralManagerDidUpdateState>")
        delegate?.nxCentralManagerDidUpdateState(central)
        switch central.state {
        case .poweredOn:
            if scan {
                print("central start scan <NXCentralManager:centralManagerDidUpdateState>")
                central.scanForPeripherals(withServices: NXUUID.services, options: nil)
            }
        case .poweredOff:
            return
        default:
            return
        }
        
    }
    
    
    public func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        print("will restore state function call <NXCentralManager:centralManager>")
        
    }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("discovered peripheral <NXCentralManager:centralManager>")
        if (peripheral == NXPeripheral.getInstance().peripheral) {
            central.connect(peripheral, options: nil)
            return
        }
        
        let localName = advertisementData[CBAdvertisementDataLocalNameKey] as! String
        let isConnectable = advertisementData[CBAdvertisementDataIsConnectable] as! NSNumber
        
        let newDiscoveredDevice = BLEDevice(name: localName, isConnectable: isConnectable.boolValue, peripheral: peripheral)
        
        delegate?.nxCentralManager?(central, didDiscover: newDiscoveredDevice)
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("device connected <NXCentralManager:centralManager>")
        central.stopScan()
        NXPeripheral.getInstance().peripheralConnect(peripheral)
        delegate?.nxCentralManager(central, didConnect: peripheral)
        MessageBarView().hide()
        
        
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        
        
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        print("device disconnected <NXCentralManager:centralManager>")
        if central.state == .poweredOn {
            central.scanForPeripherals(withServices: NXUUID.services, options: nil)
        }
        MessageBarView().showWarning(message: "Error: No Bluetooth connection")
        
        let timerQueue = DispatchQueue(label: "com.ninixco.timer", attributes: .concurrent)
        timerQueue.async {
            NXPeripheral.getInstance().timer.invalidate()
        }
    }
    
    public func connect(selected device: BLEDevice) {
        print("send connect request to device <NXCentralManager:connect>")
        centralManager.connect(device.peripheral, options: nil)
    }
    
    public static func getInstance() -> NXCentralManager {
        return self.instance
    }
    
    
}


