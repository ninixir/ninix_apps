//
//  SecondPageViewController.swift
//  ninix_final
//
//  Created by Ali on 12/28/16.
//  Copyright © 2016 ninix. All rights reserved.
//

import UIKit
import IBAnimatable

class SecondPageViewController: UIViewController {

    @IBOutlet weak var image: AnimatableImageView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        image.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        image.alpha = 1
        image.slideFade(.in, direction: .down)
        
    }

}
