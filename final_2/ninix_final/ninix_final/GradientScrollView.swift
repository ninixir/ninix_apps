//
//  GradientScrollView.swift
//  ninix_final
//
//  Created by Ali on 1/11/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import IBAnimatable

class GradientScrollView: AnimatableScrollView {
    
    var gradientColor: CAGradientLayer!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.gradientColor = ColorPallete.primaryGradient
        self.gradientColor.frame = self.bounds
        self.layer.insertSublayer(gradientColor, at: 0)
        self.backgroundColor = UIColor.clear
        
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(GradientScrollView.viewTapped(gesture:)))
        doubleTapGesture.numberOfTapsRequired = 2
        self.addGestureRecognizer(doubleTapGesture)
        
    }
    
    func viewTapped(gesture: UITapGestureRecognizer) {
        self.endEditing(true)
    }
    
}
